#define UNITTEST 0
#define DEBUGFLAG 1
#include "hpack.h"

#include <array>
#include <string>
#include <vector>
#include <chrono>
#include <memory>
#include <fstream>
#include <functional>

#if DEBUGFLAG == 1
#define RESULTCHECK 1
#endif // DEBUGFLAG


const size_t DefaultHeaderCapacity = 1 << 13;
const size_t DefaultBufferCapacity = 1 << 13;
const unsigned long long DeEnBaseLoopTime = 250;

typedef std::vector<std::pair<std::string,std::string> > HeaderVec;

std::array<const char*, 12>BenchDataFileName = {"bench/template_parameters.data","bench/Gophercon.data","bench/Hlp.data","bench/Cppvec.data","bench/content_type.data","bench/example_domains.data","bench/clang_command.data","bench/cc_sa.data","bench/go_http.data","bench/HttplightSitemap.data","bench/NginxSitemap.data","bench/HaproxySitemap.data"};

struct HeaderVecPair {
    HeaderVec req, resp;
    HeaderVecPair():req(), resp() {
        ;
    }
    HeaderVecPair(const HeaderVec &q,const HeaderVec &p):req(q), resp(p) {
        ;
    }
};

struct FuncNameFunc{
public:
    const char *funcName;
    std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int )> func;
    FuncNameFunc(const char *funcname, std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int )> f):funcName(funcname), func(f) {
    ;
}

};

std::string String2Hex(const std::string &s) {
    std::string r;
    const unsigned char trtable[17] = "0123456789abcdef";
    for(const unsigned char c : s) {
        r.append(1, trtable[(c & 0xf0) >> 4]);
        r.append(1, trtable[c & 0x0f]);
    }
    return r;
}

bool OctetAll(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
    bool isend = false;
    KeyValPair kvp;
    std::string tmpstr;

    switch(stage) {
    case 0:
        for(size_t i = 0; i<256; ++i) {
            tmpstr.append(1, (unsigned char)i);
            if((i+1) % 16 == 0) {
                kvp.first.assign(tmpstr.begin(), tmpstr.begin() + (tmpstr.size()/2));
                kvp.second.assign(tmpstr.begin() + (tmpstr.size()/2),tmpstr.end());
                reqheader.push_back(kvp);
                resphrader.push_back(kvp);
                tmpstr.clear();
                kvp.first.clear();
                kvp.second.clear();
            }
        }
        break;
    case 1:
        for(size_t i = 0; i<256; ++i) {
            tmpstr.assign(8, (unsigned char)i);
            kvp.first.assign(tmpstr.begin(),tmpstr.end());
            kvp.second.assign(tmpstr.begin(),tmpstr.end());
            reqheader.push_back(kvp);
            resphrader.push_back(kvp);
        }
        break;
    default:
        isend = true;
        break;
    }
    return isend;
}

bool LargeKvp(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
    bool isend = false;
    KeyValPair kvp;

    switch(stage) {
    case 0:
    case 1:
    case 2:
        for(size_t i = 0; i< 1024 * (stage + 1); ++i) {
            kvp.first.append(1, (unsigned char)((i % 94) + 32));
            kvp.second.append(1, (unsigned char)((i % 94) + 32));
        }
        for(size_t i = 0; i<2; ++i) {
            reqheader.push_back(kvp);
            resphrader.push_back(kvp);
        }
        break;
    default:
        isend = true;
        break;
    }
    return isend;
}

bool EmptyKeyValPair(std::vector<KeyValPair> &reqheader, std::vector<KeyValPair> &resphrader, int stage) {
    bool isend = false;

    switch(stage) {
    case 0:
        reqheader.push_back(std::make_pair<std::string, std::string>("accept-charset",""));
        reqheader.push_back(std::make_pair<std::string, std::string>("accept-encoding",""));
        reqheader.push_back(std::make_pair<std::string, std::string>(":method",""));
        reqheader.push_back(std::make_pair<std::string, std::string>(":status",""));
        resphrader.push_back(std::make_pair<std::string, std::string>("accept-charset",""));
        resphrader.push_back(std::make_pair<std::string, std::string>("accept-encoding",""));
        resphrader.push_back(std::make_pair<std::string, std::string>(":method",""));
        resphrader.push_back(std::make_pair<std::string, std::string>(":status",""));
        break;
    case 1:
        //return true;
        reqheader.push_back(std::make_pair<std::string, std::string>("",""));
        resphrader.push_back(std::make_pair<std::string, std::string>("",""));
        break;
    case 2:
        for(size_t i =0; i< 12; ++i) {
            reqheader.push_back(std::make_pair<std::string, std::string>("",""));
            resphrader.push_back(std::make_pair<std::string, std::string>("",""));
        }
        break;
    case 3:
        reqheader.push_back(std::make_pair<std::string, std::string>("","http"));
        reqheader.push_back(std::make_pair<std::string, std::string>("","https"));
        reqheader.push_back(std::make_pair<std::string, std::string>("","206"));
        reqheader.push_back(std::make_pair<std::string, std::string>("","XYZ"));
        resphrader.push_back(std::make_pair<std::string, std::string>("","http"));
        resphrader.push_back(std::make_pair<std::string, std::string>("","https"));
        resphrader.push_back(std::make_pair<std::string, std::string>("","206"));
        resphrader.push_back(std::make_pair<std::string, std::string>("","XYZ"));
        break;
    /*
    case 4:
        break;
    case 5:
        break;
    */
    default:
        isend = true;
        break;
    }
    return isend;
}

void PrintVecHeaderVecPair(std::vector<std::shared_ptr<HeaderVecPair> > hlst) {
    for(auto i : hlst) {
        printf("Request\n");
        for(auto j : i->req) {
            printf("%s : %s\n", j.first.c_str(), j.second.c_str());
            //printf("%s : %s\n", String2Hex(j.first).c_str(), String2Hex(j.second).c_str());
        }
        printf("Respones\n");
        for(auto j : i->resp) {
            printf("%s : %s\n", j.first.c_str(), j.second.c_str());
            //printf("%s : %s\n", String2Hex(j.first).c_str(), String2Hex(j.second).c_str());
        }
    }
    return;
}

bool ParserKeyVal(std::string &line,const size_t linecn,HeaderVec &vec) {
    bool escape, exist;
    size_t poscn;
    std::string keystr,valuestr,*wstr;
    unsigned char stat;

    stat = 0;
    poscn = 0;
    exist = false;
    line += " ";
    wstr = &keystr;
    escape = false;
    for(char c : line) {
        switch(c) {
        case ' ':
            if(stat == 1 || stat == 3) {
                if(escape) {
                    escape = false;
                    wstr->append(1, '\\');
                }
                wstr->append(1, ' ');
            } else {
                ;
            }
            break;
        case '"':
            if(stat == 0 || stat == 2) {
                stat += 1;
                if(stat == 3) {
                    wstr = &valuestr;
                }
            } else if(stat == 1 || stat == 3) {
                if(escape) {
                    escape = false;
                    wstr->append(1, '"');
                } else {
                    stat += 1;
                    if(stat == 4) {
                        exist = true;
                    }
                }
            }
            break;
        case '\\':
            if(stat == 1 || stat == 3) {
                escape = true;
            } else {
                exist = true;
                printf("Line %lu Pos %lu Unexpect \\.\n", linecn, poscn);
            }
            break;
        default:
            if(stat == 1 || stat == 3) {
                if(escape) {
                    escape = false;
                    wstr->append(1, '\\');
                }
                wstr->append(1, c);
            } else {
                exist = true;
                printf("Line %lu Pos %lu Unexpect '%c'.\n", linecn, poscn, c);
            }
            break;
        }
        if(exist) {
            break;
        }
        ++poscn;
    }
    if(stat == 4) {
        vec.push_back(std::make_pair<std::string,std::string>(keystr.c_str(), valuestr.c_str()));
        return true;
    } else {
        printf("Line %lu stop at Unexpect status(%u).\n", linecn, stat);
        return false;
    }
}

bool LoadBenchmarkData(std::string filename, std::vector<std::shared_ptr<HeaderVecPair> > &hlst) {
    bool exit, readfn;
    size_t linecn;
    char funcname[256];
    HeaderVec *vec;
    std::string line;
    std::ifstream fd;
    std::shared_ptr<HeaderVecPair> hvp;

    fd.open(filename, std::ios::in);
    if(!fd.is_open()) {
        printf("Can not open %s.\n", filename.c_str());
    }

    vec = nullptr;
    exit = false;
    linecn = 0;
    readfn = false;
    while(!fd.eof()) {
        ++linecn;
        std::getline(fd, line);
        if(line.size() == 0) {
            continue;
        }
        switch(line.at(0)) {
        case '#':
            break;
        case 'F':
            if(! (sscanf(line.c_str(), "FunctionName %255s", funcname) == 1)) {
                exit = true;
                printf("Line %lu not follow \"FunctionName %%255s\" format(%s).\n", linecn, line.c_str());
            }
            if(readfn) {
                exit = true;
                printf("Line %lu only allow one function in file.\n", linecn);
            }
            vec = nullptr;
            readfn = true;
            break;
        case 'Q':
            if(hvp) {
                hlst.push_back(hvp);
            }
            hvp = std::make_shared<HeaderVecPair>();
            vec = &(hvp->req);
            break;
        case 'P':
            if(!hvp) {
                exit = true;
                printf("Line %lu Use \"FunctionName %%255s\" before P.\n", linecn);
            }
            vec = &(hvp->resp);
            break;
        case '"':
            if(vec == nullptr) {
                exit = true;
                printf("Line %lu Use Q or P before \"%%8191s\"\"%%8191s\".\n", linecn);
            }
            if(!ParserKeyVal(line, linecn, *vec)) {
                printf("Line %lu not follow \"%%8191s\"\"%%8191s\" format(%s).\n", linecn, line.c_str());
            }
            break;
        default:
            printf("Line %lu Unknow(%s).\n", linecn, line.c_str());
        }
        if(exit) {
            break;
        }
    }
    if(!fd.eof()) {
        return false;
    }
    //PrintVecHeaderVecPair(hlst);
    return true;
}

void PerpareHeader(std::vector<std::shared_ptr<HeaderVecPair> > &hlst, std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int )> funcptr) {
    int stage;
    HeaderVec qheader, pheader;
    std::shared_ptr<HeaderVecPair> hvp;

    stage = 0;
    hvp = std::make_shared<HeaderVecPair>();
    while(!funcptr(hvp->req,hvp->resp, stage++)) {
        hlst.push_back(hvp);
        hvp = std::make_shared<HeaderVecPair>();
    }

    return;
}
typedef std::pair<const char *, std::function<bool (std::vector<KeyValPair>&, std::vector<KeyValPair>&, int )> > FuncNameFuncPair;

bool StageBenchmarkItem(std::string &funcname, std::vector<std::shared_ptr<HeaderVecPair> > &hlst, int stage) {
    const std::array<FuncNameFunc, 3> loadFuncLst = {FuncNameFunc("EmptyKeyValPair",EmptyKeyValPair),FuncNameFunc("OctetAll",OctetAll),FuncNameFunc("LargeKvp",LargeKvp)};
    if(stage < loadFuncLst.size()) {
        funcname.assign(loadFuncLst.at(stage).funcName);
        PerpareHeader(hlst, loadFuncLst.at(stage).func);
        return true;
    }
    stage = stage - loadFuncLst.size();
    if(stage < BenchDataFileName.size()) {
        funcname.assign(BenchDataFileName.at(stage));
        LoadBenchmarkData(BenchDataFileName.at(stage),hlst);
        return true;
    }
    return false;
}

void DeEnFailCheck(const HeaderVec &f, const HeaderVec &b,const std::string &failat,HpackStatus enstat,HpackStatus destat ) {
    if(enstat != HpackStatus::Success) {
        printf("Hpack Encode fail %s at %s.\n", Hpack::HpackStatus2String(enstat).c_str(), failat.c_str());
        return;
    }
    if(destat != HpackStatus::Success) {
        printf("Hpack Decode fail %s at %s.\n", Hpack::HpackStatus2String(destat).c_str(), failat.c_str());
        return;
    }
    if(f.size() != b.size()) {
        printf("Hpack DeEn header not same length En %lu De %lu  at %s.\n", f.size(), b.size(), failat.c_str());
        return;
    }
    if(f != b) {
        printf("Hpack DeEn %s Fail.\n", failat.c_str());
        for(size_t i = 0; i < f.size(); ++i) {
            if(f.at(i) != b.at(i)) {
                //KeyValPairPrint(f.at(i));
                //KeyValPairPrint(b.at(i));
                printf("Fail\n");
                printf("%s : %s\n", String2Hex(f.at(i).first).c_str(), String2Hex(f.at(i).second).c_str());
                printf("%s : %s\n", String2Hex(b.at(i).first).c_str(), String2Hex(b.at(i).second).c_str());
            } else {
                printf("Pass\n");
                printf("%s : %s\n", String2Hex(f.at(i).first).c_str(), String2Hex(f.at(i).second).c_str());
            }
        }
    }
    return;
}

inline unsigned long DeEnBase(const std::vector<std::shared_ptr<HeaderVecPair> > &hlst) {
    unsigned long durmill;
    HeaderVec qheader, pheader;
    HpackStatus enstat, destat;
    Hpack reqs, resps,reqr, respr;
    std::vector<unsigned char> qbuff, pbuff;
    std::chrono::steady_clock::time_point start;
    qheader.reserve(DefaultHeaderCapacity);
    pheader.reserve(DefaultHeaderCapacity);
    qbuff.reserve(DefaultBufferCapacity);
    pbuff.reserve(DefaultBufferCapacity);
    start = std::chrono::steady_clock::now();
    for(unsigned long long i = 0; i < DeEnBaseLoopTime; ++i) {
        for(const std::shared_ptr<HeaderVecPair>  &hpair : hlst) {
            enstat = reqs.Encoder(Hpack::MakeHpWBuffer(qbuff),hpair->req);
            destat = reqr.Decoder(Hpack::MakeHpRBuffer(qbuff), qheader);
#if RESULTCHECK
            DeEnFailCheck(hpair->req, qheader, "Request", enstat,destat);
#endif // RESULTCHECK
            enstat = resps.Encoder(Hpack::MakeHpWBuffer(pbuff), hpair->resp);
            destat = respr.Decoder(Hpack::MakeHpRBuffer(pbuff), pheader);
#if RESULTCHECK
            DeEnFailCheck(hpair->resp, pheader, "Response",enstat,destat);
#endif // RESULTCHECK
            qbuff.clear();
            pbuff.clear();
            qheader.clear();
            pheader.clear();
        }
    }
    durmill = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count();
    return durmill;
}

void BenchmarkItem() {
    int stage;
    double millisecond;
    std::string funcorfilename;
    const unsigned long long loopcn = 1;
    std::vector<std::shared_ptr<HeaderVecPair> > hlst;
    std::chrono::steady_clock::time_point start;

    stage = 0;
    while(StageBenchmarkItem(funcorfilename, hlst,stage) && stage < 20) {
        stage++;
        millisecond = 0;
        for(unsigned long long i = 0; i < loopcn; ++i) {
            millisecond += DeEnBase(hlst);
        }
        printf("%s run %llu time in %lfms.\n", funcorfilename.c_str(),DeEnBaseLoopTime,millisecond / loopcn);
        hlst.clear();
    }
    return;
}

int main() {
    BenchmarkItem();
    return 0;
}
