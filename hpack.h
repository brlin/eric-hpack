#ifndef HPACK_H
#define HPACK_H

enum HpackStatus : int;
enum HeaderFieldRepresentation : int;
class HeaderFieldInfo;
class HpRBuffer;
class HpWBuffer;
class Hpack;

#include "hpack.cpp"

#endif //HPACK_H
