#include <stdio.h>
#include <stdint.h>

// cpp header
#include <string>
#include <vector>
#include <memory>
#include <utility>

#ifndef UNITTEST
#define UNITTEST 1
#endif // UNITTEST
#ifndef DEBUGFLAG
#define DEBUGFLAG 1
#endif //DEBUGFLAG


typedef int_fast16_t Integer2byte;
typedef uint_fast64_t Uinteger5byte;
typedef std::pair<std::string,std::string> KeyValPair;

// need to keep order
const unsigned char IndexedHeaderField = 0x80;
const unsigned char LiteralHeaderFieldWithIncrementalIndexing = 0x40;
const unsigned char DynamicTableSizeUpdate = 0x20;
const unsigned char LiteralHeaderFieldNeverIndexed = 0x10;
const unsigned char LiteralHeaderFieldWithoutIndexing = 0x00;

const Integer2byte SymbolEOS = 256;
const Integer2byte SymbolInitial = 257;
const unsigned char HuffmanLiteralFlag = 0x80;
const unsigned char BitOfUinteger5Byte = (sizeof(Uinteger5byte) / sizeof(unsigned char)) * 8;
// default
const Integer2byte SETTINGS_HEADER_TABLE_SIZE = 4096;

// config
const bool DecodeStringLiteralOctet = true;
const bool HpackDecoderCheckParameter = false;
const bool LiteralDecodeCheckLiteralPad = true;

enum StringLiteralEncodeWay : int {
    EOctet,
    EHuffman,
    EShortest
};

enum HpackStatus : int {
    Success,
    IntegerOverFlow,
    TableIndexZero,
    TableIndeNotExist,
    StringLiteralOctet,
    LiteralPadNotEOSMsb,
    LiteralContainEOS,
    LiteralPadLongThan7Bit,
    RBuffEndAtIntegerDecode,
    RBuffEndAtLiteralDecode,
    RBuffEndAtHeaderField,
    RBuffZeroLength,
    WBuffErrIntegerEecode,
    WBuffErrLiteralEecode,
    WBuffErrHeaderField,
    HpackEncodeFieldRepresent,
    DecodeSwitchEnd,
};

enum HeaderFieldRepresentation : int {
    RInitial,
    RIndexedHeaderField,
    RLiteralHeaderFieldWithIncrementalIndexing,
    RDynamicTableSizeUpdate,
    RLiteralHeaderFieldNeverIndexed,
    RLiteralHeaderFieldWithoutIndexing,
    RLiteralHeaderFieldAlwaysIndex
};

class HeaderFieldInfo {
  public:
    size_t evictCounter;
    HeaderFieldRepresentation representation;
    HeaderFieldInfo():evictCounter(0), representation(HeaderFieldRepresentation::RInitial) {
        ;
    }
};

class HuffmanCode {
  public:
    const Uinteger5byte lsbValue;
    const unsigned char lengthOfBit;
    HuffmanCode(const Uinteger5byte lsbvalue,const unsigned char lengthofbit):lsbValue(lsbvalue),lengthOfBit(lengthofbit) {
        ;
    }
};

const size_t StaticTableSize = 62;
const KeyValPair StaticTable[StaticTableSize] = {\
                                                 std::make_pair<const std::string, const std::string>("TableIndexZero", "TableIndexZero"),\
                                                 std::make_pair<const std::string, const std::string>(":authority", ""),\
                                                 std::make_pair<const std::string, const std::string>(":method", "GET"),\
                                                 std::make_pair<const std::string, const std::string>(":method", "POST"),\
                                                 std::make_pair<const std::string, const std::string>(":path", "/"),\
                                                 std::make_pair<const std::string, const std::string>(":path", "/index.html"),\
                                                 std::make_pair<const std::string, const std::string>(":scheme", "http"),\
                                                 std::make_pair<const std::string, const std::string>(":scheme", "https"),\
                                                 std::make_pair<const std::string, const std::string>(":status", "200"),\
                                                 std::make_pair<const std::string, const std::string>(":status", "204"),\
                                                 std::make_pair<const std::string, const std::string>(":status", "206"),\
                                                 std::make_pair<const std::string, const std::string>(":status", "304"),\
                                                 std::make_pair<const std::string, const std::string>(":status", "400"),\
                                                 std::make_pair<const std::string, const std::string>(":status", "404"),\
                                                 std::make_pair<const std::string, const std::string>(":status", "500"),\
                                                 std::make_pair<const std::string, const std::string>("accept-charset", ""),\
                                                 std::make_pair<const std::string, const std::string>("accept-encoding", "gzip, deflate"),\
                                                 std::make_pair<const std::string, const std::string>("accept-language", ""),\
                                                 std::make_pair<const std::string, const std::string>("accept-ranges", ""),\
                                                 std::make_pair<const std::string, const std::string>("accept", ""),\
                                                 std::make_pair<const std::string, const std::string>("access-control-allow-origin", ""),\
                                                 std::make_pair<const std::string, const std::string>("age", ""),\
                                                 std::make_pair<const std::string, const std::string>("allow", ""),\
                                                 std::make_pair<const std::string, const std::string>("authorization", ""),\
                                                 std::make_pair<const std::string, const std::string>("cache-control", ""),\
                                                 std::make_pair<const std::string, const std::string>("content-disposition", ""),\
                                                 std::make_pair<const std::string, const std::string>("content-encoding", ""),\
                                                 std::make_pair<const std::string, const std::string>("content-language", ""),\
                                                 std::make_pair<const std::string, const std::string>("content-length", ""),\
                                                 std::make_pair<const std::string, const std::string>("content-location", ""),\
                                                 std::make_pair<const std::string, const std::string>("content-range", ""),\
                                                 std::make_pair<const std::string, const std::string>("content-type", ""),\
                                                 std::make_pair<const std::string, const std::string>("cookie", ""),\
                                                 std::make_pair<const std::string, const std::string>("date", ""),\
                                                 std::make_pair<const std::string, const std::string>("etag", ""),\
                                                 std::make_pair<const std::string, const std::string>("expect", ""),\
                                                 std::make_pair<const std::string, const std::string>("expires", ""),\
                                                 std::make_pair<const std::string, const std::string>("from", ""),\
                                                 std::make_pair<const std::string, const std::string>("host", ""),\
                                                 std::make_pair<const std::string, const std::string>("if-match", ""),\
                                                 std::make_pair<const std::string, const std::string>("if-modified-since", ""),\
                                                 std::make_pair<const std::string, const std::string>("if-none-match", ""),\
                                                 std::make_pair<const std::string, const std::string>("if-range", ""),\
                                                 std::make_pair<const std::string, const std::string>("if-unmodified-since", ""),\
                                                 std::make_pair<const std::string, const std::string>("last-modified", ""),\
                                                 std::make_pair<const std::string, const std::string>("link", ""),\
                                                 std::make_pair<const std::string, const std::string>("location", ""),\
                                                 std::make_pair<const std::string, const std::string>("max-forwards", ""),\
                                                 std::make_pair<const std::string, const std::string>("proxy-authenticate", ""),\
                                                 std::make_pair<const std::string, const std::string>("proxy-authorization", ""),\
                                                 std::make_pair<const std::string, const std::string>("range", ""),\
                                                 std::make_pair<const std::string, const std::string>("referer", ""),\
                                                 std::make_pair<const std::string, const std::string>("refresh", ""),\
                                                 std::make_pair<const std::string, const std::string>("retry-after", ""),\
                                                 std::make_pair<const std::string, const std::string>("server", ""),\
                                                 std::make_pair<const std::string, const std::string>("set-cookie", ""),\
                                                 std::make_pair<const std::string, const std::string>("strict-transport-security", ""),\
                                                 std::make_pair<const std::string, const std::string>("transfer-encoding", ""),\
                                                 std::make_pair<const std::string, const std::string>("user-agent", ""),\
                                                 std::make_pair<const std::string, const std::string>("vary", ""),\
                                                 std::make_pair<const std::string, const std::string>("via", ""),\
                                                 std::make_pair<const std::string, const std::string>("www-authenticate", ""),\
                                                };

class HpRBufferCache;

unsigned char __HuffmanSwitchfffffff00000b30(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffffe00000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffffc00000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffffa00000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffff800000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffff600000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffff400000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffff200000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffff000000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffffee00000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffffec00000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffffea00000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffffe800000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffffe600000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffffe400000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffffe200000b28(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffffe000000b27(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffffc000000b27(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffffbc00000b27(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffffa000000b26(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffff8000000b26(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffff6000000b25(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffff4000000b24(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffff2000000b24(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffff0000000b24(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffee000000b24(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffec000000b24(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffea000000b24(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffe0000000b23(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffc0000000b23(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffbc000000b23(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffb8000000b23(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffb4000000b23(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffb0000000b23(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffffa0000000b22(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffff80000000b22(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffff60000000b22(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffff58000000b22(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffff50000000b22(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffff48000000b22(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffff40000000b21(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffff20000000b21(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffff00000000b21(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffee0000000b21(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffec0000000b20(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffea0000000b20(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffe80000000b20(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffe60000000b20(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfffe00000000b19(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfff800000000b15(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfff000000000b14(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffc000000000b13(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchffa000000000b12(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchff8000000000b11(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchff4000000000b11(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchff0000000000b10(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchfe0000000000b10(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchf80000000000b8(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchf00000000000b7(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitche80000000000b7(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitche00000000000b7(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchd80000000000b7(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchd00000000000b7(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchc80000000000b7(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchc00000000000b7(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchb80000000000b7(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitchb00000000000b6(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitcha80000000000b6(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitcha00000000000b6(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitch980000000000b6(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitch900000000000b6(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitch880000000000b6(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitch800000000000b6(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitch780000000000b6(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitch700000000000b6(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitch680000000000b6(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitch600000000000b6(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitch580000000000b6(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitch500000000000b6(HpRBufferCache &rbc,Integer2byte &sym);
unsigned char __HuffmanSwitch0b5(HpRBufferCache &rbc,Integer2byte &sym);

const HuffmanCode HuffmanCodeTable[257] = {\
                                           HuffmanCode(0x1ff8ul, 13),\
                                           HuffmanCode(0x7fffd8ul, 23),\
                                           HuffmanCode(0xfffffe2ul, 28),\
                                           HuffmanCode(0xfffffe3ul, 28),\
                                           HuffmanCode(0xfffffe4ul, 28),\
                                           HuffmanCode(0xfffffe5ul, 28),\
                                           HuffmanCode(0xfffffe6ul, 28),\
                                           HuffmanCode(0xfffffe7ul, 28),\
                                           HuffmanCode(0xfffffe8ul, 28),\
                                           HuffmanCode(0xffffeaul, 24),\
                                           HuffmanCode(0x3ffffffcul, 30),\
                                           HuffmanCode(0xfffffe9ul, 28),\
                                           HuffmanCode(0xfffffeaul, 28),\
                                           HuffmanCode(0x3ffffffdul, 30),\
                                           HuffmanCode(0xfffffebul, 28),\
                                           HuffmanCode(0xfffffecul, 28),\
                                           HuffmanCode(0xfffffedul, 28),\
                                           HuffmanCode(0xfffffeeul, 28),\
                                           HuffmanCode(0xfffffeful, 28),\
                                           HuffmanCode(0xffffff0ul, 28),\
                                           HuffmanCode(0xffffff1ul, 28),\
                                           HuffmanCode(0xffffff2ul, 28),\
                                           HuffmanCode(0x3ffffffeul, 30),\
                                           HuffmanCode(0xffffff3ul, 28),\
                                           HuffmanCode(0xffffff4ul, 28),\
                                           HuffmanCode(0xffffff5ul, 28),\
                                           HuffmanCode(0xffffff6ul, 28),\
                                           HuffmanCode(0xffffff7ul, 28),\
                                           HuffmanCode(0xffffff8ul, 28),\
                                           HuffmanCode(0xffffff9ul, 28),\
                                           HuffmanCode(0xffffffaul, 28),\
                                           HuffmanCode(0xffffffbul, 28),\
                                           HuffmanCode(0x14ul, 6),\
                                           HuffmanCode(0x3f8ul, 10),\
                                           HuffmanCode(0x3f9ul, 10),\
                                           HuffmanCode(0xffaul, 12),\
                                           HuffmanCode(0x1ff9ul, 13),\
                                           HuffmanCode(0x15ul, 6),\
                                           HuffmanCode(0xf8ul, 8),\
                                           HuffmanCode(0x7faul, 11),\
                                           HuffmanCode(0x3faul, 10),\
                                           HuffmanCode(0x3fbul, 10),\
                                           HuffmanCode(0xf9ul, 8),\
                                           HuffmanCode(0x7fbul, 11),\
                                           HuffmanCode(0xfaul, 8),\
                                           HuffmanCode(0x16ul, 6),\
                                           HuffmanCode(0x17ul, 6),\
                                           HuffmanCode(0x18ul, 6),\
                                           HuffmanCode(0x0ul, 5),\
                                           HuffmanCode(0x1ul, 5),\
                                           HuffmanCode(0x2ul, 5),\
                                           HuffmanCode(0x19ul, 6),\
                                           HuffmanCode(0x1aul, 6),\
                                           HuffmanCode(0x1bul, 6),\
                                           HuffmanCode(0x1cul, 6),\
                                           HuffmanCode(0x1dul, 6),\
                                           HuffmanCode(0x1eul, 6),\
                                           HuffmanCode(0x1ful, 6),\
                                           HuffmanCode(0x5cul, 7),\
                                           HuffmanCode(0xfbul, 8),\
                                           HuffmanCode(0x7ffcul, 15),\
                                           HuffmanCode(0x20ul, 6),\
                                           HuffmanCode(0xffbul, 12),\
                                           HuffmanCode(0x3fcul, 10),\
                                           HuffmanCode(0x1ffaul, 13),\
                                           HuffmanCode(0x21ul, 6),\
                                           HuffmanCode(0x5dul, 7),\
                                           HuffmanCode(0x5eul, 7),\
                                           HuffmanCode(0x5ful, 7),\
                                           HuffmanCode(0x60ul, 7),\
                                           HuffmanCode(0x61ul, 7),\
                                           HuffmanCode(0x62ul, 7),\
                                           HuffmanCode(0x63ul, 7),\
                                           HuffmanCode(0x64ul, 7),\
                                           HuffmanCode(0x65ul, 7),\
                                           HuffmanCode(0x66ul, 7),\
                                           HuffmanCode(0x67ul, 7),\
                                           HuffmanCode(0x68ul, 7),\
                                           HuffmanCode(0x69ul, 7),\
                                           HuffmanCode(0x6aul, 7),\
                                           HuffmanCode(0x6bul, 7),\
                                           HuffmanCode(0x6cul, 7),\
                                           HuffmanCode(0x6dul, 7),\
                                           HuffmanCode(0x6eul, 7),\
                                           HuffmanCode(0x6ful, 7),\
                                           HuffmanCode(0x70ul, 7),\
                                           HuffmanCode(0x71ul, 7),\
                                           HuffmanCode(0x72ul, 7),\
                                           HuffmanCode(0xfcul, 8),\
                                           HuffmanCode(0x73ul, 7),\
                                           HuffmanCode(0xfdul, 8),\
                                           HuffmanCode(0x1ffbul, 13),\
                                           HuffmanCode(0x7fff0ul, 19),\
                                           HuffmanCode(0x1ffcul, 13),\
                                           HuffmanCode(0x3ffcul, 14),\
                                           HuffmanCode(0x22ul, 6),\
                                           HuffmanCode(0x7ffdul, 15),\
                                           HuffmanCode(0x3ul, 5),\
                                           HuffmanCode(0x23ul, 6),\
                                           HuffmanCode(0x4ul, 5),\
                                           HuffmanCode(0x24ul, 6),\
                                           HuffmanCode(0x5ul, 5),\
                                           HuffmanCode(0x25ul, 6),\
                                           HuffmanCode(0x26ul, 6),\
                                           HuffmanCode(0x27ul, 6),\
                                           HuffmanCode(0x6ul, 5),\
                                           HuffmanCode(0x74ul, 7),\
                                           HuffmanCode(0x75ul, 7),\
                                           HuffmanCode(0x28ul, 6),\
                                           HuffmanCode(0x29ul, 6),\
                                           HuffmanCode(0x2aul, 6),\
                                           HuffmanCode(0x7ul, 5),\
                                           HuffmanCode(0x2bul, 6),\
                                           HuffmanCode(0x76ul, 7),\
                                           HuffmanCode(0x2cul, 6),\
                                           HuffmanCode(0x8ul, 5),\
                                           HuffmanCode(0x9ul, 5),\
                                           HuffmanCode(0x2dul, 6),\
                                           HuffmanCode(0x77ul, 7),\
                                           HuffmanCode(0x78ul, 7),\
                                           HuffmanCode(0x79ul, 7),\
                                           HuffmanCode(0x7aul, 7),\
                                           HuffmanCode(0x7bul, 7),\
                                           HuffmanCode(0x7ffeul, 15),\
                                           HuffmanCode(0x7fcul, 11),\
                                           HuffmanCode(0x3ffdul, 14),\
                                           HuffmanCode(0x1ffdul, 13),\
                                           HuffmanCode(0xffffffcul, 28),\
                                           HuffmanCode(0xfffe6ul, 20),\
                                           HuffmanCode(0x3fffd2ul, 22),\
                                           HuffmanCode(0xfffe7ul, 20),\
                                           HuffmanCode(0xfffe8ul, 20),\
                                           HuffmanCode(0x3fffd3ul, 22),\
                                           HuffmanCode(0x3fffd4ul, 22),\
                                           HuffmanCode(0x3fffd5ul, 22),\
                                           HuffmanCode(0x7fffd9ul, 23),\
                                           HuffmanCode(0x3fffd6ul, 22),\
                                           HuffmanCode(0x7fffdaul, 23),\
                                           HuffmanCode(0x7fffdbul, 23),\
                                           HuffmanCode(0x7fffdcul, 23),\
                                           HuffmanCode(0x7fffddul, 23),\
                                           HuffmanCode(0x7fffdeul, 23),\
                                           HuffmanCode(0xffffebul, 24),\
                                           HuffmanCode(0x7fffdful, 23),\
                                           HuffmanCode(0xffffecul, 24),\
                                           HuffmanCode(0xffffedul, 24),\
                                           HuffmanCode(0x3fffd7ul, 22),\
                                           HuffmanCode(0x7fffe0ul, 23),\
                                           HuffmanCode(0xffffeeul, 24),\
                                           HuffmanCode(0x7fffe1ul, 23),\
                                           HuffmanCode(0x7fffe2ul, 23),\
                                           HuffmanCode(0x7fffe3ul, 23),\
                                           HuffmanCode(0x7fffe4ul, 23),\
                                           HuffmanCode(0x1fffdcul, 21),\
                                           HuffmanCode(0x3fffd8ul, 22),\
                                           HuffmanCode(0x7fffe5ul, 23),\
                                           HuffmanCode(0x3fffd9ul, 22),\
                                           HuffmanCode(0x7fffe6ul, 23),\
                                           HuffmanCode(0x7fffe7ul, 23),\
                                           HuffmanCode(0xffffeful, 24),\
                                           HuffmanCode(0x3fffdaul, 22),\
                                           HuffmanCode(0x1fffddul, 21),\
                                           HuffmanCode(0xfffe9ul, 20),\
                                           HuffmanCode(0x3fffdbul, 22),\
                                           HuffmanCode(0x3fffdcul, 22),\
                                           HuffmanCode(0x7fffe8ul, 23),\
                                           HuffmanCode(0x7fffe9ul, 23),\
                                           HuffmanCode(0x1fffdeul, 21),\
                                           HuffmanCode(0x7fffeaul, 23),\
                                           HuffmanCode(0x3fffddul, 22),\
                                           HuffmanCode(0x3fffdeul, 22),\
                                           HuffmanCode(0xfffff0ul, 24),\
                                           HuffmanCode(0x1fffdful, 21),\
                                           HuffmanCode(0x3fffdful, 22),\
                                           HuffmanCode(0x7fffebul, 23),\
                                           HuffmanCode(0x7fffecul, 23),\
                                           HuffmanCode(0x1fffe0ul, 21),\
                                           HuffmanCode(0x1fffe1ul, 21),\
                                           HuffmanCode(0x3fffe0ul, 22),\
                                           HuffmanCode(0x1fffe2ul, 21),\
                                           HuffmanCode(0x7fffedul, 23),\
                                           HuffmanCode(0x3fffe1ul, 22),\
                                           HuffmanCode(0x7fffeeul, 23),\
                                           HuffmanCode(0x7fffeful, 23),\
                                           HuffmanCode(0xfffeaul, 20),\
                                           HuffmanCode(0x3fffe2ul, 22),\
                                           HuffmanCode(0x3fffe3ul, 22),\
                                           HuffmanCode(0x3fffe4ul, 22),\
                                           HuffmanCode(0x7ffff0ul, 23),\
                                           HuffmanCode(0x3fffe5ul, 22),\
                                           HuffmanCode(0x3fffe6ul, 22),\
                                           HuffmanCode(0x7ffff1ul, 23),\
                                           HuffmanCode(0x3ffffe0ul, 26),\
                                           HuffmanCode(0x3ffffe1ul, 26),\
                                           HuffmanCode(0xfffebul, 20),\
                                           HuffmanCode(0x7fff1ul, 19),\
                                           HuffmanCode(0x3fffe7ul, 22),\
                                           HuffmanCode(0x7ffff2ul, 23),\
                                           HuffmanCode(0x3fffe8ul, 22),\
                                           HuffmanCode(0x1ffffecul, 25),\
                                           HuffmanCode(0x3ffffe2ul, 26),\
                                           HuffmanCode(0x3ffffe3ul, 26),\
                                           HuffmanCode(0x3ffffe4ul, 26),\
                                           HuffmanCode(0x7ffffdeul, 27),\
                                           HuffmanCode(0x7ffffdful, 27),\
                                           HuffmanCode(0x3ffffe5ul, 26),\
                                           HuffmanCode(0xfffff1ul, 24),\
                                           HuffmanCode(0x1ffffedul, 25),\
                                           HuffmanCode(0x7fff2ul, 19),\
                                           HuffmanCode(0x1fffe3ul, 21),\
                                           HuffmanCode(0x3ffffe6ul, 26),\
                                           HuffmanCode(0x7ffffe0ul, 27),\
                                           HuffmanCode(0x7ffffe1ul, 27),\
                                           HuffmanCode(0x3ffffe7ul, 26),\
                                           HuffmanCode(0x7ffffe2ul, 27),\
                                           HuffmanCode(0xfffff2ul, 24),\
                                           HuffmanCode(0x1fffe4ul, 21),\
                                           HuffmanCode(0x1fffe5ul, 21),\
                                           HuffmanCode(0x3ffffe8ul, 26),\
                                           HuffmanCode(0x3ffffe9ul, 26),\
                                           HuffmanCode(0xffffffdul, 28),\
                                           HuffmanCode(0x7ffffe3ul, 27),\
                                           HuffmanCode(0x7ffffe4ul, 27),\
                                           HuffmanCode(0x7ffffe5ul, 27),\
                                           HuffmanCode(0xfffecul, 20),\
                                           HuffmanCode(0xfffff3ul, 24),\
                                           HuffmanCode(0xfffedul, 20),\
                                           HuffmanCode(0x1fffe6ul, 21),\
                                           HuffmanCode(0x3fffe9ul, 22),\
                                           HuffmanCode(0x1fffe7ul, 21),\
                                           HuffmanCode(0x1fffe8ul, 21),\
                                           HuffmanCode(0x7ffff3ul, 23),\
                                           HuffmanCode(0x3fffeaul, 22),\
                                           HuffmanCode(0x3fffebul, 22),\
                                           HuffmanCode(0x1ffffeeul, 25),\
                                           HuffmanCode(0x1ffffeful, 25),\
                                           HuffmanCode(0xfffff4ul, 24),\
                                           HuffmanCode(0xfffff5ul, 24),\
                                           HuffmanCode(0x3ffffeaul, 26),\
                                           HuffmanCode(0x7ffff4ul, 23),\
                                           HuffmanCode(0x3ffffebul, 26),\
                                           HuffmanCode(0x7ffffe6ul, 27),\
                                           HuffmanCode(0x3ffffecul, 26),\
                                           HuffmanCode(0x3ffffedul, 26),\
                                           HuffmanCode(0x7ffffe7ul, 27),\
                                           HuffmanCode(0x7ffffe8ul, 27),\
                                           HuffmanCode(0x7ffffe9ul, 27),\
                                           HuffmanCode(0x7ffffeaul, 27),\
                                           HuffmanCode(0x7ffffebul, 27),\
                                           HuffmanCode(0xffffffeul, 28),\
                                           HuffmanCode(0x7ffffecul, 27),\
                                           HuffmanCode(0x7ffffedul, 27),\
                                           HuffmanCode(0x7ffffeeul, 27),\
                                           HuffmanCode(0x7ffffeful, 27),\
                                           HuffmanCode(0x7fffff0ul, 27),\
                                           HuffmanCode(0x3ffffeeul, 26),\
                                           HuffmanCode(0x3ffffffful, 30),\
                                          };

#if DEBUGFLAG == 1
char *LsbByte(unsigned char ii) {
    static char rlt[9];
    unsigned char bcmp = 1;
    for(Integer2byte i=0; i<8; ++i) {
        rlt[7-i] = (bcmp & ii) == 0 ? '0' : '1';
        bcmp = bcmp << 1;
    }
    rlt[8] = '\0';
    return rlt;
}

void KeyValPairPrint(const KeyValPair &kvp) {
    printf("%s:%s\n", kvp.first.c_str(),kvp.second.c_str());
    return;
}
#endif // DEBUGFLAG

#define XXXERRORCHECKUNEXPECTSTAT(stat) {  \
printf("%s:%d XXXErrorCheckUnexpectStatus(%s).\n",__FILE__ , __LINE__, HpackStatus2CString((stat))); \
return (stat);                             \
}

inline const char *HpackStatus2CString(const HpackStatus stat) {
    switch(stat) {
    case HpackStatus::Success:
        return "HpackStatus::Success";
    case HpackStatus::IntegerOverFlow:
        return "HpackStatus::IntegerOverFlow";
    case HpackStatus::RBuffEndAtIntegerDecode:
        return "HpackStatus::RBuffEndAtIntegerDecode";
    case HpackStatus::TableIndexZero:
        return "HpackStatus::TableIndexZero";
    case HpackStatus::TableIndeNotExist:
        return "HpackStatus::TableIndeNotExist";
    case HpackStatus::StringLiteralOctet:
        return "HpackStatus::StringLiteralOctet";
    case HpackStatus::LiteralPadNotEOSMsb:
        return "HpackStatus::LiteralPadNotEOSMsb";
    case HpackStatus::LiteralContainEOS:
        return "HpackStatus::LiteralContainEOS";
    case HpackStatus::LiteralPadLongThan7Bit:
        return "HpackStatus::LiteralPadLongThan7Bit";
    case HpackStatus::RBuffEndAtLiteralDecode:
        return "HpackStatus::RBuffEndAtLiteralDecode";
    case HpackStatus::RBuffEndAtHeaderField:
        return "HpackStatus::RBuffEndAtHeaderField";
    case HpackStatus::RBuffZeroLength:
        return "HpackStatus::RBuffZeroLength";
    case HpackStatus::WBuffErrIntegerEecode:
        return "HpackStatus::WBuffErrIntegerEecode";
    case HpackStatus::WBuffErrLiteralEecode:
        return "HpackStatus::WBuffErrLiteralEecode";
    case HpackStatus::WBuffErrHeaderField:
        return "HpackStatus::WBuffErrHeaderField";
    case HpackStatus::HpackEncodeFieldRepresent:
        return "HpackStatus::HpackEncodeFieldRepresent";
    case HpackStatus::DecodeSwitchEnd:
        return "HpackStatus::DecodeSwitchEnd";
    default:
        return "Unexpect status in HpackStatus2String.";
    }
}

class HpRBuffer {
    // 0 length not allowed
  public:
    virtual ~HpRBuffer() = default;
    virtual bool Next() =0;
    virtual size_t Size() = 0;
    virtual unsigned char Currrent() = 0;
};

class VecHpRBuffer:public HpRBuffer {
  public:
    size_t pos;
    const std::vector<unsigned char> &ss;
    VecHpRBuffer(const std::vector<unsigned char> &ss): pos(0), ss(ss) {
        ;
    }
    unsigned char Currrent() override {
        return ss[pos];
    }
    bool Next() override {
        if(pos < ss.size() - 1) {
            pos+=1;
            return true;
        }
        return false;
    }
    size_t Size() override {
        return ss.size();
    }
};


class ArrHpRBuffer:public HpRBuffer {
  public:
    size_t size;
    const unsigned char *ptr;
    const unsigned char *end;
    ArrHpRBuffer(const unsigned char *start, size_t sizebyte):size(sizebyte),ptr(start), end(start + ((sizebyte / sizeof(unsigned char)) - 1)) {
        ;
    }
    bool Next()override {
        if(ptr == end) {
            return false;
        }
        ptr+=1;
        return true;
    }
    size_t Size()override {
        return size;
    }
    unsigned char Currrent()override {
        return *ptr;
    }
    /*
    const ArrHpRBuffer& operator = (const ArrHpRBuffer &r){
        ptr = r.ptr;
        end = r.end;
        size = r.size;
        return *this;
    }*/
};

class HpWBuffer {
  public:
    size_t maxSize;
    std::vector<unsigned char> &s;
    HpWBuffer(std::vector<unsigned char> &ss):maxSize(16384),s(ss) {
        ;
    }
    HpWBuffer(std::vector<unsigned char> &ss, size_t maxsize):maxSize(maxsize),s(ss) {
        ;
    }
    unsigned char OrCurrrent(unsigned char i) {
        if (s.size() == 0) {
            s.push_back(i);
        } else {
            s[(s.size() - 1)] = (s[(s.size() - 1)] | i);
        }
        return s[s.size() - 1];
    }
    bool Append(unsigned char i) {
        if(s.size() <= maxSize) {
            s.push_back(i);
            return true;
        }
        return false;
    }
    size_t Size() {
        return s.size();
    }
};

#if DEBUGFLAG == 1
void BufferPrint(const std::vector<unsigned char> &s) {
    size_t i;
    printf("Size%3lu\n", s.size());
    for(i = 0; i<s.size(); ++i) {
        printf("%s ",LsbByte(s[i]));
        if((i +1) % 4 == 0) {
            printf("\n");
        }
    }
    if((i) % 4 != 0) {
        printf("\n");
    }
}
void BufferFromHex(const std::string &str,std::vector<unsigned char> &s) {
    bool firstc = true;
    unsigned char tmp = 0;
    for(char c : str) {
        if(c == ' ') {
            continue;
        } else if(c >= '0' && c <= '9') {
            tmp += c - '0';
        } else if(c >= 'a' && c <= 'f') {
            tmp += (c - 'a') + 10;
        } else if(c >= 'A' && c <= 'F') {
            tmp += (c - 'A') + 10;
        } else {
            printf("BufferFromHex Unexpect char %d.\n", (int)c);
            continue;
        }
        if(firstc) {
            tmp = tmp << 4;
            firstc = false;
        } else {
            firstc = true;
            s.push_back(tmp);
            tmp = 0;
        }
    }
    if(! firstc) {
        printf("BufferFromHex Hexstr must be an even.\n");
    }
}
#endif // DEBUGFLAG

inline HpackStatus IntegerEncode(HpWBuffer &w,const Integer2byte prefix,Uinteger5byte i) {
#if DEBUGFLAG == 1
    if (prefix > 8) {
        printf("IntegerEncode prefix(%d) only supper less then 8.", prefix);
    }
#endif // DEBUGFLAG

    Uinteger5byte tmp;
    if(i >= (1U << (Uinteger5byte)prefix) - 1) {
        w.OrCurrrent((1 << prefix) - 1);
        i=i- ((1 << prefix) - 1);
        do {
            tmp = i & 0x007f;
            i = i >> 7;
            if(i > 0) {
                tmp = tmp | 0x0080;
            }
            if(!w.Append((unsigned char)tmp)) {
                return HpackStatus::WBuffErrIntegerEecode;
            }
        } while(i != 0);
    } else {
        w.OrCurrrent((unsigned char)i);
    }
    return HpackStatus::Success;
}

#define INTEGERENCODEERRORCHECK(stat)        \
switch(stat){                                \
    case HpackStatus::Success:               \
        break;                               \
    case HpackStatus::WBuffErrIntegerEecode: \
        return stat;                         \
    default:                                 \
        XXXERRORCHECKUNEXPECTSTAT(stat)      \
}

inline HpackStatus IntegerDecode(HpRBuffer &r,const Integer2byte prefix, Uinteger5byte &i) {
#if DEBUGFLAG == 1
    if (prefix > 8) {
        printf("IntegerDecode prefix(%d) only supper less then 8.", prefix);
    }
#endif // DEBUGFLAG

    Uinteger5byte tmp;
    unsigned char cn = 0;
    if((r.Currrent() & ((1 << prefix)-1)) == ((1 << prefix)-1)) {
        i = (1 << prefix)-1;
        do {
            if(!r.Next()) {
                return HpackStatus::RBuffEndAtIntegerDecode;
            }
            //printf("%d %lu %d\n",cn, i,(r.Currrent() & 0x7f));
            tmp = (r.Currrent() & 0x7f);
            i = i + (tmp << (7 * cn));

            if(cn++ > (unsigned char)(sizeof(i) / sizeof(unsigned char))) {
                return HpackStatus::IntegerOverFlow;
            }
        } while((r.Currrent() & 0x80) != 0);
    } else {
        i = (Uinteger5byte)(r.Currrent() & ((1 << prefix)-1));
    }
    return HpackStatus::Success;
}

#define INTEGERDECODEERRORCHECK(stat)           \
switch(stat) {                                  \
    case HpackStatus::Success:                  \
        break;                                  \
    case HpackStatus::IntegerOverFlow :         \
    case HpackStatus::RBuffEndAtIntegerDecode:  \
        return (stat);                          \
    default:                                    \
        XXXERRORCHECKUNEXPECTSTAT(stat)         \
}

class HpRBufferCache {
    HpRBuffer &r;
    Uinteger5byte strLength, strpos = 0, huffmanCode = 0;
    unsigned char huffmanCodeBit = 0, cacheBit = 0;
  public:
    static const unsigned char CacheSuccess = 1;
    static const unsigned char StopAtRBuffEnd = 2;
    static const unsigned char StopAtStringLength = 3;

    HpRBufferCache(HpRBuffer &_r, Uinteger5byte strlength): r(_r), strLength(strlength) {
        ;
    }
    inline unsigned char ReadUntilBit(const unsigned char thisbit) {
        Uinteger5byte tmp5byte_t;
#if DEBUGFLAG == 1
        if(thisbit >= BitOfUinteger5Byte) {
            printf("HpRBufferCache::ReadUntilBit thisbit(%u) >= BitOfUinteger5Byte(%u).\n", thisbit, BitOfUinteger5Byte);
        }
        if(cacheBit < huffmanCodeBit) {
            printf("HpRBufferCache::ReadUntilBit cacheBit(%u) < huffmanCodeBit(%u).\n", cacheBit, huffmanCodeBit);
        }
#endif // DEBUGFLAG
        if(thisbit <= cacheBit) {
            huffmanCodeBit = thisbit;
            //printf("ReadUntilBit Update huffmanCodeBit(%u).\n", huffmanCodeBit);
        } else {
            while(cacheBit < thisbit) {
                if(strpos++ >= strLength) {
                    return StopAtStringLength;
                }
                if(!r.Next()) {
                    return StopAtRBuffEnd;
                }
                tmp5byte_t = (Uinteger5byte)r.Currrent();
                tmp5byte_t = tmp5byte_t << (BitOfUinteger5Byte - 8 - cacheBit);
                //printf("ReadUntilBit tmp5byte_t(%llx) BitOfUinteger5Byte(%u) shift(%u).\n", tmp5byte_t, BitOfUinteger5Byte, (BitOfUinteger5Byte - 8 - cacheBit));
#if DEBUGFLAG == 1
                if((huffmanCode | tmp5byte_t) != huffmanCode + tmp5byte_t) {
                    printf("HpRBufferCache::ReadUntilBit shift(%u bit) Error huffmanCode(0x%llx),tmp5byte_t(0x%llx).\n",BitOfUinteger5Byte - 8 - cacheBit, huffmanCode, tmp5byte_t );
                }
#endif // DEBUGFLAG
                cacheBit += 8;
                huffmanCode = huffmanCode | tmp5byte_t;
                //printf("ReadUntilBit strpos(%llx) tmp5byte_t(%llx) huffmanCode(%llx) cacheBit(%u) huffmanCodeBit(%u).\n", strpos, tmp5byte_t, huffmanCode, cacheBit, huffmanCodeBit);
            }
            huffmanCodeBit = thisbit;
        }
        return CacheSuccess;
    }
    inline void NextHuffmanCode() {
#if DEBUGFLAG == 1
        if(cacheBit < huffmanCodeBit) {
            printf("HpRBufferCache::NextHuffmanCode cacheBit(%u) < huffmanCodeBit(%u).\n", cacheBit, huffmanCodeBit);
        }
#endif // DEBUGFLAG
        huffmanCode = huffmanCode << huffmanCodeBit;
        cacheBit = cacheBit - huffmanCodeBit;
        huffmanCodeBit = 0;
        return;
    }
    inline Uinteger5byte HuffmanCode() {
        Uinteger5byte tmp = 1;
        tmp = (huffmanCodeBit == 0 ? (Uinteger5byte)0 : ((tmp << huffmanCodeBit) - 1) << (BitOfUinteger5Byte - huffmanCodeBit)) ;
        return huffmanCode & tmp;
    }
    inline HpackStatus CheckStopAtStringLength() {
        Uinteger5byte tmp = 1;
        if(cacheBit > 7) {
            return HpackStatus::LiteralPadLongThan7Bit;
        }
        if((huffmanCode >> (BitOfUinteger5Byte - cacheBit)) != ((cacheBit == 0) ? (Uinteger5byte)0 : (tmp << cacheBit) - 1)) {
            return HpackStatus::LiteralPadNotEOSMsb;
        }
        return HpackStatus::Success;
    }
};

inline HpackStatus StringLiteralDecodeHuffman(HpRBuffer &r, std::string &str,Uinteger5byte strLength) {
    Integer2byte sym;
    unsigned char rbcstat;
    HpRBufferCache rbc(r,strLength);

    while(1) {
#if DEBUGFLAG == 1
        sym = SymbolInitial;
#endif // DEBUGFLAG
        rbcstat = __HuffmanSwitch0b5(rbc, sym);
        switch(rbcstat) {
        case HpRBufferCache::CacheSuccess:
            break;
        case HpRBufferCache::StopAtRBuffEnd:
            return HpackStatus::RBuffEndAtLiteralDecode;
        case HpRBufferCache::StopAtStringLength:
            goto ENDATSTRLENGTH;
#if DEBUGFLAG == 1
        default:
            printf("StringLiteralDecodeHuffman __HuffmanSwitch return unknown status(%d).\n", rbcstat);
#endif // DEBUGFLAG
        }
        rbc.NextHuffmanCode();
        switch(sym) {
        case SymbolInitial:
            printf("StringLiteralDecodeHuffman __HuffmanSwitch Destruction.\n");
        case SymbolEOS:
            return HpackStatus::LiteralContainEOS;
        default:
            str.push_back(sym);
        }
    }
ENDATSTRLENGTH:
    if(LiteralDecodeCheckLiteralPad) {
        return rbc.CheckStopAtStringLength();
    } else {
        return HpackStatus::Success;
    }
}

inline HpackStatus StringLiteralDecodeOctet(HpRBuffer &r, std::string &str,const Uinteger5byte strLength) {
    if(DecodeStringLiteralOctet) {
        str.reserve(strLength);
        for(Uinteger5byte i = 0; i<strLength; ++i) {
            if(r.Next()) {
                str.append(1, r.Currrent());
            } else {
                return HpackStatus::RBuffEndAtLiteralDecode;
            }
        }
        return HpackStatus::Success;
    } else {
        return HpackStatus::StringLiteralOctet;
    }
}

// str new and clean bt callee
inline HpackStatus StringLiteralDecode(HpRBuffer &r, std::string &str) {
    bool isHuffman;
    HpackStatus stat;
    Uinteger5byte strLength;

    if((r.Currrent() & HuffmanLiteralFlag) == HuffmanLiteralFlag) {
        isHuffman = true;
    } else {
        isHuffman = false;
    }
    stat = IntegerDecode(r, 7, strLength);
    INTEGERDECODEERRORCHECK(stat);
    if(isHuffman) {
        stat = StringLiteralDecodeHuffman(r, str, strLength);
    } else {
        stat = StringLiteralDecodeOctet(r, str, strLength);
    }

    return stat;
}

#define LITERALDECODEERRORCHECK(stat)          \
switch(stat){                                  \
    case HpackStatus::Success:                 \
        break;                                 \
    case HpackStatus::LiteralPadNotEOSMsb:     \
    case HpackStatus::LiteralContainEOS:       \
    case HpackStatus::LiteralPadLongThan7Bit:  \
    case HpackStatus::RBuffEndAtLiteralDecode: \
    case HpackStatus::StringLiteralOctet:      \
    case HpackStatus::IntegerOverFlow :        \
    case HpackStatus::RBuffEndAtIntegerDecode: \
        return (stat);                         \
    default:                                   \
        XXXERRORCHECKUNEXPECTSTAT(stat)        \
}

inline HpackStatus StringLiteralEncodeHuffman(HpWBuffer &w,const std::string &str) {
    static const unsigned char bitofUinteger5byte = (sizeof(Uinteger5byte) / sizeof(unsigned char)) * 8;

    unsigned char wa;
    Uinteger5byte msb;
    Uinteger5byte left;
    unsigned char shift;
    unsigned char wbyte;
    unsigned char bitoftotal;

    left = 0;
    shift = 0;
    for(const unsigned char c : str) {
        bitoftotal= HuffmanCodeTable[c].lengthOfBit + shift;
        wbyte = bitoftotal / 8;
        msb = (HuffmanCodeTable[c].lsbValue << (bitofUinteger5byte - (HuffmanCodeTable[c].lengthOfBit + shift))) | left;
        //printf("wbyte %d bitoftotal %d shift %d msb %lx left %lx\n", wbyte, bitoftotal,shift, msb, left);
        for(unsigned char j = 0; j<wbyte; ++j) {
            if(!w.Append((unsigned char)(msb >> (bitofUinteger5byte - 8)))) {
                return HpackStatus::WBuffErrLiteralEecode;
            }
            msb = msb << 8;
        }
        left = msb;
        shift = bitoftotal % 8;
    }
    if(shift != 0) {
        wa = ((left >> (bitofUinteger5byte - 8)) | ((1 << (8 - shift)) - 1));
        if(!w.Append(wa)) {
            return HpackStatus::WBuffErrLiteralEecode;
        }
        //printf("StringLiteralEncodeHuffman pad shift %d msb %u wa %d.\n", shift, (left >> (bitofUinteger5byte - 8)), wa);
    }
    return HpackStatus::Success;
}

template<StringLiteralEncodeWay SLEF>
inline HpackStatus StringLiteralEncode(HpWBuffer &w,const std::string &str) {
    HpackStatus stat;
    Uinteger5byte encodelen;
    Uinteger5byte huffstrlen = 0;
    unsigned char encodetypebit;

    if((SLEF == StringLiteralEncodeWay::EShortest) || (SLEF == StringLiteralEncodeWay::EHuffman)) {
        for(const unsigned char c : str) {
            huffstrlen += HuffmanCodeTable[c].lengthOfBit;
        }
        huffstrlen = (huffstrlen / 8) + ((huffstrlen % 8) == 0 ? 0 : 1);
    }

    if(SLEF == StringLiteralEncodeWay::EShortest) {
        if(huffstrlen < str.size()) {
            encodelen = huffstrlen;
            encodetypebit = HuffmanLiteralFlag;
        } else {
            encodelen = str.size();
            encodetypebit = 0;
        }
    } else if(SLEF == StringLiteralEncodeWay::EHuffman) {
        encodelen = huffstrlen;
        encodetypebit = HuffmanLiteralFlag;
    } else if(SLEF == StringLiteralEncodeWay::EOctet) {
        encodelen = str.size();
        encodetypebit = 0;
    }

    w.OrCurrrent(encodetypebit);
    stat = IntegerEncode(w, 7, encodelen);
    INTEGERENCODEERRORCHECK(stat);
    if(encodetypebit == HuffmanLiteralFlag) {
        stat = StringLiteralEncodeHuffman(w, str);
    } else {
        for(const unsigned char c : str) {
            if(!w.Append(c)) {
                return HpackStatus::WBuffErrLiteralEecode;
            }
        }
    }
    return HpackStatus::Success;
}

#define LITERALENCODEERRORCHECK(stat)          \
switch(stat){                                  \
    case HpackStatus::Success:                 \
        break;                                 \
    case HpackStatus::WBuffErrLiteralEecode:   \
        return (stat);                         \
    default:                                   \
        XXXERRORCHECKUNEXPECTSTAT(stat)        \
}

class IndexTable {
  private:
    size_t dynamicTableSize;
    // Must not zero
    Integer2byte dynamicTableMaxSize;
    std::vector<KeyValPair> dynamicTable;
    inline size_t CalculatSize(const KeyValPair &kvp) {
        // RFC 7541 4.1. Calculating Table Size
        return kvp.first.size() + kvp.second.size() + 32u;
    }
    inline size_t DynamicTableEvict() {
        size_t evictcn = 0;
        std::vector<KeyValPair>::iterator iter;
        while(dynamicTableSize > dynamicTableMaxSize) {
#if DEBUGFLAG == 1
            if(dynamicTable.size() == 0 ) {
                printf("IndexTable::DynamicTableEvict dynamicTable and dynamicTableSize out of sync dynamicTable::size %lu dynamicTableSize %lu.\n", dynamicTable.size(), dynamicTableSize);
            }
#endif // DEBUGFLAG
            evictcn +=1;
            iter = dynamicTable.begin() + (dynamicTable.size() - 1);
            dynamicTableSize -= CalculatSize(*iter);
            dynamicTable.pop_back();
        }
        return evictcn;
    }
  public:
    IndexTable(const Integer2byte dynamictablemaxsize):dynamicTableSize(0),  dynamicTableMaxSize(dynamictablemaxsize),dynamicTable() {
        dynamicTable.reserve(dynamictablemaxsize / sizeof(KeyValPair));
    }
    // Index start at 1
    inline HpackStatus at(size_t i, KeyValPair &kvp) {
        size_t dynpos;
        if (i == 0) {
            return  HpackStatus::TableIndexZero;
        }
        if(i < StaticTableSize) {
            kvp = StaticTable[i];
            return HpackStatus::Success;
        }
        dynpos = i - StaticTableSize;
        if(dynpos < dynamicTable.size()) {
            kvp = dynamicTable[dynpos];
            return HpackStatus::Success;
        }
        printf("IndexTable::at(%lu) TableIndeNotExist.\n", i);
        return HpackStatus::TableIndeNotExist;
    }
    // StaticTable has priority in search. If two tables have the same key, StaticTable will be used.
    // StaticTableSize is the real length of StaticTable
    inline size_t find(const KeyValPair &kvp, bool &onlymatchkey) {
        size_t indx = 0;

        onlymatchkey = false;
        for(size_t i = 1; i < StaticTableSize; ++i) {
            if(StaticTable[i].first == kvp.first) {
                if(StaticTable[i].second == kvp.second) {
                    return i;
                }
                if(indx == 0) {
                    indx = i;
                }
            }
        }
        for(size_t i = 0; i < dynamicTable.size(); ++i) {
            if(dynamicTable[i].first == kvp.first) {
                if(dynamicTable[i].second == kvp.second) {
                    return StaticTableSize + i;
                }
                if(indx == 0) {
                    indx = StaticTableSize + i;
                }
            }
        }
        onlymatchkey = true;
        return indx;
    }
    inline void DynamicTableInsert(const KeyValPair &kvp,size_t &evictcn) {
        dynamicTableSize += CalculatSize(kvp);
        dynamicTable.insert(dynamicTable.begin(), kvp);
        evictcn = DynamicTableEvict();
    }
    inline Integer2byte DynamicTableMaxSize() {
        return dynamicTableMaxSize;
    }
    inline size_t DynamicTableSize() {
        return dynamicTableSize;
    }
#if DEBUGFLAG == 1
    std::vector<KeyValPair> DynamicTable() {
        return dynamicTable;
    }
#endif // DEBUGFLAG
};

#define INDEXTABLEATEERRORCHECK(stat)          \
switch(stat) {                                 \
    case HpackStatus::Success:                 \
        break;                                 \
    case HpackStatus::TableIndexZero:          \
    case HpackStatus::TableIndeNotExist:       \
        return stat;                           \
    default:                                   \
        XXXERRORCHECKUNEXPECTSTAT(stat)        \
}

#if DEBUGFLAG == 1
void IndexTablePrint(IndexTable &inxt) {
    for(KeyValPair kvp : inxt.DynamicTable()) {
        KeyValPairPrint(kvp);
    }
}
#endif // DEBUGFLAG

class Hpack {
    size_t headerBufferMax = 4096;
    IndexTable indexTable;

    inline HpackStatus _DecoderLoop(HpRBuffer &stream, KeyValPair &kvp, HeaderFieldInfo &hfdi) {
        bool readkey;
        size_t indx;
        HpackStatus stat;

        indx = 0;
        readkey = false;
        if((stream.Currrent() & IndexedHeaderField) == IndexedHeaderField) {
            hfdi.representation = HeaderFieldRepresentation::RIndexedHeaderField;
            stat = IntegerDecode(stream, 7, indx);
            INTEGERDECODEERRORCHECK(stat);
            stat = indexTable.at(indx, kvp);
            INDEXTABLEATEERRORCHECK(stat);
            return HpackStatus::Success;
        } else if((stream.Currrent() & LiteralHeaderFieldWithIncrementalIndexing) == LiteralHeaderFieldWithIncrementalIndexing) {
            hfdi.representation = HeaderFieldRepresentation::RLiteralHeaderFieldWithIncrementalIndexing;
            stat = IntegerDecode(stream, 6, indx);
            INTEGERDECODEERRORCHECK(stat);
        } else if((stream.Currrent() & DynamicTableSizeUpdate) == DynamicTableSizeUpdate) {
            hfdi.representation = HeaderFieldRepresentation::RDynamicTableSizeUpdate;
            return HpackStatus::Success;
        } else if((stream.Currrent() & LiteralHeaderFieldNeverIndexed) == LiteralHeaderFieldNeverIndexed) {
            hfdi.representation = HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed;
            stat = IntegerDecode(stream, 4, indx);
            INTEGERDECODEERRORCHECK(stat);
        } else {
            hfdi.representation = HeaderFieldRepresentation::RLiteralHeaderFieldWithoutIndexing;
            stat = IntegerDecode(stream, 4, indx);
            INTEGERDECODEERRORCHECK(stat);
        }
        if(indx == 0) {
            readkey = true;
        } else {
            stat = indexTable.at(indx, kvp);
            INDEXTABLEATEERRORCHECK(stat);
            kvp.second.clear();
        }
        if (readkey) {
            if(!stream.Next()) {
                return HpackStatus::RBuffEndAtHeaderField;
            }
            stat = StringLiteralDecode(stream, kvp.first);
            LITERALDECODEERRORCHECK(stat);
        }
        if(!stream.Next()) {
            return HpackStatus::RBuffEndAtHeaderField;
        }
        stat = StringLiteralDecode(stream, kvp.second);
        LITERALDECODEERRORCHECK(stat);
        if (hfdi.representation == HeaderFieldRepresentation::RLiteralHeaderFieldWithIncrementalIndexing) {
            indexTable.DynamicTableInsert(kvp, hfdi.evictCounter);
        }
        return HpackStatus::Success;
    }
    template<bool HFIF>
    inline HpackStatus _Decoder(HpRBuffer &stream, std::vector<KeyValPair> &header, std::vector<HeaderFieldInfo> *headerfieldinfo) {
        KeyValPair kvp;
        HpackStatus stat;
        HeaderFieldInfo hfdi;
#if DEBUGFLAG == 1
        size_t linecounter;
#endif // DEBUGFLAG
        if(stream.Size() > headerBufferMax) {
            ;
        }
        if(HpackDecoderCheckParameter) {
            if(stream.Size() == 0) {
                return HpackStatus::RBuffZeroLength;
            }
        }

        do {
            if((stat = _DecoderLoop(stream,kvp, hfdi)) != HpackStatus::Success) {
#if DEBUGFLAG == 1
                errorLine =  linecounter;
#endif // DEBUGFLAG
                return stat;
            }
            header.push_back(kvp);
            if(HFIF) {
                headerfieldinfo->push_back(hfdi);
            }
            kvp.first.clear();
            kvp.second.clear();
#if DEBUGFLAG == 1
            linecounter++;
#endif // DEBUGFLAG
        } while(stream.Next());
        return HpackStatus::Success;
    }

    inline HpackStatus _EncoderLoopIndexed(HpWBuffer &stream,size_t index) {
        // Indexed Header Field
        HpackStatus stat;
        stream.Append(IndexedHeaderField);
        stat = IntegerEncode(stream, 7, index);
        INTEGERENCODEERRORCHECK(stat);
        return HpackStatus::Success;
    }
    template<StringLiteralEncodeWay SLEF>
    inline HpackStatus _EncoderLoopLiteralHeader(HpWBuffer &stream,const KeyValPair &kvp,size_t index,HeaderFieldRepresentation hfrp) {
        HpackStatus stat;
        unsigned char fieldrepr, indexbit;
        switch(hfrp) {
        case HeaderFieldRepresentation::RLiteralHeaderFieldWithIncrementalIndexing:
            indexbit = 6;
            fieldrepr = LiteralHeaderFieldWithIncrementalIndexing;
            break;
        case HeaderFieldRepresentation::RLiteralHeaderFieldWithoutIndexing:
            indexbit = 4;
            fieldrepr = LiteralHeaderFieldWithoutIndexing;
            break;
        case HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed:
            indexbit = 4;
            fieldrepr = LiteralHeaderFieldNeverIndexed;
            break;
        default:
            return HpackStatus::HpackEncodeFieldRepresent;
        }
        if(!stream.Append(fieldrepr)) {
            return HpackStatus::WBuffErrHeaderField;
        }
        if(index == 0) {
            if(!stream.Append(0)) {
                return HpackStatus::WBuffErrHeaderField;
            }
            stat = StringLiteralEncode<SLEF>(stream, kvp.first);
            LITERALENCODEERRORCHECK(stat);
        } else {
            stat = IntegerEncode(stream, indexbit, index);
            INTEGERENCODEERRORCHECK(stat);
        }
        if(!stream.Append(0)) {
            return HpackStatus::WBuffErrHeaderField;
        }
        stat = StringLiteralEncode<SLEF>(stream, kvp.second);
        LITERALENCODEERRORCHECK(stat);
        return HpackStatus::Success;
    }
    template<StringLiteralEncodeWay SLEF>
    inline HpackStatus _EncoderLoopAlwaysIndex(HpWBuffer &stream, const KeyValPair &kvp, size_t index, bool onlymatchkey, HeaderFieldInfo &hfdi) {
        HpackStatus stat = HpackStatus::Success;
        //printf("_EncoderLoopAlwaysIndex index %lu onlymatchkey %d.\n",index, onlymatchkey);
        if(index != 0 && !onlymatchkey) {
            stat = _EncoderLoopIndexed(stream, index);
            hfdi.representation = HeaderFieldRepresentation::RIndexedHeaderField;
        } else {
            stat = _EncoderLoopLiteralHeader<SLEF>(stream, kvp, index, HeaderFieldRepresentation::RLiteralHeaderFieldWithIncrementalIndexing);
            indexTable.DynamicTableInsert(kvp, hfdi.evictCounter);
            hfdi.representation = HeaderFieldRepresentation::RLiteralHeaderFieldWithIncrementalIndexing;
        }
        return stat;
    }
    template<bool DOEXCLIDE,StringLiteralEncodeWay SLEF>
    inline HpackStatus _Encoder(HpWBuffer &stream,const std::vector<KeyValPair> &header,const std::vector<std::string> *exclude) {
        size_t indx;
        HpackStatus stat;
        bool onlymatchkey, isexclude;
        HeaderFieldInfo hfdi;

        for(const KeyValPair &kvp : header) {
            if(DOEXCLIDE) {
                isexclude = false;
                for(const std::string &excludekey : *exclude) {
                    if(kvp.first == excludekey) {
                        isexclude = true;
                        break;
                    }
                }
            }

            indx = indexTable.find(kvp, onlymatchkey);
            if(DOEXCLIDE) {
                if(isexclude) {
                    stat = _EncoderLoopLiteralHeader<SLEF>(stream, kvp, indx, HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed);
                } else {
                    stat = _EncoderLoopAlwaysIndex<SLEF>(stream, kvp, indx, onlymatchkey, hfdi);
                }
            } else {
                stat = _EncoderLoopAlwaysIndex<SLEF>(stream, kvp, indx, onlymatchkey, hfdi);
            }

            if(stat != HpackStatus::Success) {
                return stat;
            }
        }
        return HpackStatus::Success;
    }
    template<StringLiteralEncodeWay SLEF>
    inline HpackStatus _EncoderHeaderFieldInfo(HpWBuffer &stream,const std::vector<KeyValPair> &header,std::vector<HeaderFieldInfo> &hfdilst) {
        size_t indx;
        HpackStatus stat;
        bool onlymatchkey;

        std::vector<KeyValPair>::const_iterator kvpiter = header.cbegin();
        std::vector<HeaderFieldInfo>::iterator hfiter = hfdilst.begin();
        if(header.size() != hfdilst.size()) {

        }

        while(kvpiter != header.end()) {
            indx = indexTable.find(*kvpiter, onlymatchkey);
            switch(hfiter->representation) {
            case HeaderFieldRepresentation::RLiteralHeaderFieldAlwaysIndex:
                stat = _EncoderLoopAlwaysIndex<SLEF>(stream,*kvpiter, indx, onlymatchkey, (*hfiter));
                break;
            case HeaderFieldRepresentation::RLiteralHeaderFieldWithIncrementalIndexing:
            case HeaderFieldRepresentation::RLiteralHeaderFieldWithoutIndexing:
            case HeaderFieldRepresentation::RLiteralHeaderFieldNeverIndexed:
                stat = _EncoderLoopLiteralHeader<SLEF>(stream, *kvpiter, indx, hfiter->representation);
                break;
            default:
                stat = HpackStatus::HpackEncodeFieldRepresent;
                break;
            }
            if(stat != HpackStatus::Success) {
                return stat;
            }
            hfiter++;
            kvpiter++;
        }
        return HpackStatus::Success;
    }
  public:
#if DEBUGFLAG
    size_t errorLine = 0;
#endif // DEBUGFLAG
    Hpack() : indexTable(SETTINGS_HEADER_TABLE_SIZE) {
        ;
    }
    Hpack(unsigned int settingheadertablesize) : indexTable(settingheadertablesize) {
        ;
    }
    HpackStatus Decoder(std::shared_ptr<HpRBuffer> stream, std::vector<KeyValPair> &header) {
        return _Decoder<false>(*stream, header, nullptr);
    }
    HpackStatus Decoder(std::shared_ptr<HpRBuffer> stream, std::vector<KeyValPair> &header, std::vector<HeaderFieldInfo> &headerfieldinfo) {
        return _Decoder<true>(*stream, header, &headerfieldinfo);
    }
    HpackStatus Encoder(std::shared_ptr<HpWBuffer> stream,const std::vector<KeyValPair> &header) {
        return _Encoder<false, StringLiteralEncodeWay::EHuffman>(*stream, header, nullptr);
    }
    HpackStatus Encoder(std::shared_ptr<HpWBuffer> stream,const std::vector<KeyValPair> &header, const std::vector<std::string> &exclude) {
        return _Encoder<true, StringLiteralEncodeWay::EShortest>(*stream, header, &exclude);
    }
    HpackStatus Encoder(std::shared_ptr<HpWBuffer> stream,const std::vector<KeyValPair> &header, std::vector<HeaderFieldInfo> &hfdilst) {
        return _EncoderHeaderFieldInfo<StringLiteralEncodeWay::EShortest>(*stream, header, hfdilst);
    }
    static std::shared_ptr<HpRBuffer> MakeHpRBuffer(const std::vector<unsigned char> &ss) {
        return std::make_shared<VecHpRBuffer>(ss);
    }
    static std::shared_ptr<HpRBuffer> MakeHpRBuffer(const unsigned char *start, size_t sizebyte) {
        return std::make_shared<ArrHpRBuffer>(start, sizebyte);
    }
    static std::shared_ptr<HpWBuffer> MakeHpWBuffer(std::vector<unsigned char> &ss) {
        return std::make_shared<HpWBuffer>(ss);
    }
    static const std::string HpackStatus2String(HpackStatus stat) {
        return std::string(HpackStatus2CString(stat));
    }
#if DEBUGFLAG == 1
    IndexTable& _IndexTable() {
        return indexTable;
    }
#endif // DEBUGFLAG
};

#if UNITTEST == 1
void TestIntegerEnDe(Integer2byte prefix, Uinteger5byte i) {
    HpackStatus stat;
    std::vector<unsigned char> buff;
    HpWBuffer wb(buff);
    VecHpRBuffer rb(buff);

    printf("TestIntegerEnDe(prefix:%d, i:%lu)\n", prefix, i);
    if((stat = IntegerEncode(wb, prefix, i)) != HpackStatus::Success) {
        printf("TestIntegerEnDe fail at IntegerEncode %s.\n",HpackStatus2CString(stat));
    }
    BufferPrint(buff);
    i = 0;
    if(IntegerDecode(rb, prefix, i) == HpackStatus::Success) {
        printf("TestIntegerEnDe:%lu\n", i);
    }
}

void _TestHpXBuffer(HpRBuffer &rb) {
    do {
        printf("%s ",LsbByte(rb.Currrent()));
    } while(rb.Next());
    for(int i = 0; i<10; ++i) {
        if(rb.Next()) {
            printf("HpRBuffer not keep End.\n\n");
        }
    }
    printf("\n");
}

void TestHpXBuffer(const std::vector<unsigned char> &buff) {
    size_t i =0;
    unsigned char buffmem[1024];
    std::shared_ptr<HpRBuffer> rb = std::make_shared<VecHpRBuffer>(buff);
    printf("\nVecHpRBuffer\n");
    _TestHpXBuffer(*(rb.get()));
    i = 0;
    for( unsigned char c : buff ) {
        buffmem[i++] = c;
    }
    std::shared_ptr<ArrHpRBuffer> ra = std::make_shared<ArrHpRBuffer>(buffmem, buff.size());
    printf("ArrHpRBuffer\n");
    _TestHpXBuffer(*(ra.get()));
}

std::shared_ptr<Hpack> TestHpackDecoder(std::vector<unsigned char> &buff,std::shared_ptr<Hpack> hpack = {}) {
    if(!hpack) {
        hpack = std::make_shared<Hpack>();
    }
    std::vector<KeyValPair> header;

    printf("%s\nHeader:\n", Hpack::HpackStatus2String(hpack->Decoder(Hpack::MakeHpRBuffer(buff), header)).c_str());
    for(KeyValPair &kvp : header) {
        KeyValPairPrint(kvp);
    }
    IndexTablePrint(hpack->_IndexTable());
    buff.clear();
    return hpack;
}

std::shared_ptr<Hpack> TestHpackEncoder(std::vector<KeyValPair> &header,std::vector<unsigned char> &ans,std::shared_ptr<Hpack> hpack = {}) {
    std::vector<unsigned char> buff;

    if(!hpack) {
        hpack = std::make_shared<Hpack>();
    }
    printf("hpack::Encoder %s\n",Hpack::HpackStatus2String(hpack->Encoder(Hpack::MakeHpWBuffer(buff), header)).c_str());
    if(buff != ans) {
        printf("TestHpackEncoder not match anser.\n");
        for(const KeyValPair &kvp : header) {
            KeyValPairPrint(kvp);
        }
        BufferPrint(buff);
        BufferPrint(ans);
    }
    IndexTablePrint(hpack->_IndexTable());
    ans.clear();
    header.clear();
    return hpack;
}

std::string String2Hex(const std::string &s) {
    std::string r;
    const unsigned char trtable[17] = "0123456789abcdef";
    for(const unsigned char c : s) {
        r.append(1, trtable[(c & 0xf0) >> 4]);
        r.append(1, trtable[c & 0x0f]);
    }
    return r;
}

int main() {
    // will dose not free mem
    printf("Test LsbByte...\n");
    printf("%s\n",LsbByte(1));
    printf("%s\n",LsbByte(3));
    printf("%s\n",LsbByte(254));

    printf("Test IndexTable...\n");
    size_t evictcn;
    KeyValPair kvp;
    bool onlymatchkey;
    IndexTable inxt(SETTINGS_HEADER_TABLE_SIZE);
    inxt.at(1, kvp);
    KeyValPairPrint(kvp);
    inxt.at(2, kvp);
    if(inxt.at(StaticTableSize - 1,kvp)  == HpackStatus::TableIndeNotExist) {
        printf("IndexTable fail at not return TableIndeNotExist.\n");
    }
    kvp.first = kvp.second = std::string("0123456789");
    inxt.DynamicTableInsert(kvp, evictcn);
    inxt.at(StaticTableSize,kvp);
    KeyValPairPrint(kvp);
    IndexTablePrint(inxt);
    if(inxt.find(kvp, onlymatchkey) != StaticTableSize) {
        printf("IndexTable fail at find in DynamicTable.\n");
    }
    unsigned int headerTableSize = 104;
    IndexTable inxtt(headerTableSize);
    kvp.first = kvp.second = std::string("0123456789");
    inxtt.DynamicTableInsert(kvp, evictcn);
    inxtt.DynamicTableInsert(kvp, evictcn);
    IndexTablePrint(inxtt);
    kvp.first = kvp.second = std::string("01234567899");
    inxtt.DynamicTableInsert(kvp, evictcn);
    if (evictcn != 2) {
        printf("Not expect evictcn %lu\n", evictcn);
    }


    printf("Test HpXBuffer...\n");
    std::vector<unsigned char> buff;
    std::shared_ptr<HpWBuffer> wb = std::make_shared<HpWBuffer>(buff);
    std::shared_ptr<HpRBuffer> rb = std::make_shared<VecHpRBuffer>(buff);

    wb->OrCurrrent(0x03);
    wb->Append(0x04);
    wb->Append(0x05);
    BufferPrint(buff);
    TestHpXBuffer(buff);
    buff.clear();
    wb->OrCurrrent(0x00);
    for(unsigned char i = 1; i<255; i++) {
        wb->Append(i);
    }
    TestHpXBuffer(buff);
    buff.clear();

    printf("Test Integer Decode Encode...\n");
    TestIntegerEnDe(5,10);
    TestIntegerEnDe(5,1337);
    TestIntegerEnDe(5,31);
    TestIntegerEnDe(6,63);
    TestIntegerEnDe(4,15);
    TestIntegerEnDe(7,8);
    TestIntegerEnDe(5,654321);
    TestIntegerEnDe(6,654321);
    TestIntegerEnDe(7,654321);

    printf("Test StringLiteral Decode...\n");
    std::string str;
    BufferFromHex("0a 6375 7374 6f6d 2d6b 6579", buff);
    rb = std::make_shared<VecHpRBuffer>(buff);
    if(StringLiteralDecode(*rb, str) == HpackStatus::Success) {
        printf("%s\n", str.c_str());
    } else {
        printf("StringLiteral Decode fail.\n");
        BufferPrint(buff);
    }
    str.clear();
    buff.clear();

    BufferFromHex("8cf1e3 c2e5 f23a 6ba0 ab90 f4ff", buff);
    rb = std::make_shared<VecHpRBuffer>(buff);
    if(StringLiteralDecode(*rb, str) == HpackStatus::Success) {
        printf("%s\n", str.c_str());
    } else {
        printf("StringLiteral Decode fail.\n");
        BufferPrint(buff);
    }
    str.clear();
    buff.clear();

    printf("Test StringLiteral Encode...\n");
    wb = std::make_shared<HpWBuffer>(buff);
    rb = std::make_shared<VecHpRBuffer>(buff);
    StringLiteralEncode<StringLiteralEncodeWay::EHuffman>(*(wb.get()), "www.example.com");
    StringLiteralDecode(*(rb.get()), str);
    printf("%s\n", str.c_str());
    buff.clear();
    str.clear();

    wb = std::make_shared<HpWBuffer>(buff);
    rb = std::make_shared<VecHpRBuffer>(buff);
    StringLiteralEncode<StringLiteralEncodeWay::EOctet>(*(wb.get()), "/w/cpp/language/template_parameters");
    StringLiteralDecode(*(rb.get()), str);
    printf("%s\n", str.c_str());
    buff.clear();
    str.clear();

    wb = std::make_shared<HpWBuffer>(buff);
    rb = std::make_shared<VecHpRBuffer>(buff);
    StringLiteralEncode<StringLiteralEncodeWay::EHuffman>(*(wb.get()), "ab_testing_id=%22ee51c1ab-004a-48ac-88fa-62aafc2a39c3%22; Max-Age=31536000; Domain=substack.com; Path=/; Expires=Fri, 20 Dec 2024 07:32:01 GMT; HttpOnly; Secure; SameSite=Lax, __cf_bm=SRtuA8QBZVz8ikCsBwpOHRClxl4QQMwWaTYXeWMugyk-1703143921-1-Afd6A/Ia7gKwvluIpmSt+mgEYaAC7lbtqpwK2QyJSN0uFIa/ZVUKI9XcP0c9rjr9VJaWEU762GhbKRDG5+gpPgQ=; path=/; expires=Thu, 21-Dec-23 08:02:01 GMT; domain=.substack.com; HttpOnly; Secure; SameSite=None");
    StringLiteralDecode(*(rb.get()), str);
    printf("%s\n", str.c_str());
    buff.clear();
    str.clear();

    wb = std::make_shared<HpWBuffer>(buff);
    rb = std::make_shared<VecHpRBuffer>(buff);
    StringLiteralEncode<StringLiteralEncodeWay::EHuffman>(*(wb.get()), "\x0a\x16");
    StringLiteralDecode(*(rb.get()), str);
    BufferPrint(buff);
    printf("%s\n", String2Hex(str).c_str());
    buff.clear();
    str.clear();

    printf("Test Hpack Decoder...\n");
    BufferFromHex("400a 6375 7374 6f6d 2d6b 6579 0d63 7573", buff);
    BufferFromHex("746f 6d2d 6865 6164 6572", buff);
    TestHpackDecoder(buff);

    BufferFromHex("040c 2f73 616d 706c 652f 7061 7468", buff);
    TestHpackDecoder(buff);

    BufferFromHex("1008 7061 7373 776f 7264 0673 6563 726574", buff);
    TestHpackDecoder(buff);

    BufferFromHex("82", buff);
    TestHpackDecoder(buff);

    std::shared_ptr<Hpack> hpack;
    BufferFromHex("8286 8441 8cf1 e3c2 e5f2 3a6b a0ab 90f4ff", buff);
    hpack = TestHpackDecoder(buff);
    BufferFromHex("8286 84be 5886 a8eb 1064 9cbf", buff);
    TestHpackDecoder(buff, hpack);
    BufferFromHex("8287 85bf 4088 25a8 49e9 5ba9 7d7f 8925 a849 e95b b8e8 b4bf", buff);
    TestHpackDecoder(buff, hpack);
    buff.clear();

    printf("Test Hpack Encoder...\n");
    std::vector<KeyValPair> header;
    std::vector<unsigned char> buffans;
    BufferFromHex("8286 8441 8cf1 e3c2 e5f2 3a6b a0ab 90f4ff", buffans);
    header.push_back(std::make_pair(std::string(":method"),std::string("GET")));
    header.push_back(std::make_pair(std::string(":scheme"),std::string("http")));
    header.push_back(std::make_pair(std::string(":path"),std::string("/")));
    header.push_back(std::make_pair(std::string(":authority"),std::string("www.example.com")));
    hpack = TestHpackEncoder(header, buffans);

    BufferFromHex("8286 84be 5886 a8eb 1064 9cbf", buffans);
    header.push_back(std::make_pair(std::string(":method"),std::string("GET")));
    header.push_back(std::make_pair(std::string(":scheme"),std::string("http")));
    header.push_back(std::make_pair(std::string(":path"),std::string("/")));
    header.push_back(std::make_pair(std::string(":authority"),std::string("www.example.com")));
    header.push_back(std::make_pair(std::string("cache-control"),std::string("no-cache")));
    hpack = TestHpackEncoder(header, buffans, hpack);

    BufferFromHex("8287 85bf 4088 25a8 49e9 5ba9 7d7f 8925 a849 e95b b8e8 b4bf", buffans);
    header.push_back(std::make_pair(std::string(":method"),std::string("GET")));
    header.push_back(std::make_pair(std::string(":scheme"),std::string("https")));
    header.push_back(std::make_pair(std::string(":path"),std::string("/index.html")));
    header.push_back(std::make_pair(std::string(":authority"),std::string("www.example.com")));
    header.push_back(std::make_pair(std::string("custom-key"),std::string("custom-value")));
    hpack = TestHpackEncoder(header, buffans, hpack);

    printf("Test Hpack Encoder With Evict...\n");
    hpack = std::make_shared<Hpack>(256);
    BufferFromHex("4882 6402 5885 aec3 771a 4b61 96d0 7abe", buffans);
    BufferFromHex("9410 54d4 44a8 2005 9504 0b81 66e0 82a6", buffans);
    BufferFromHex("2d1b ff6e 919d 29ad 1718 63c7 8f0b 97c8", buffans);
    BufferFromHex("e9ae 82ae 43d3", buffans);
    header.push_back(std::make_pair(std::string(":status"),std::string("302")));
    header.push_back(std::make_pair(std::string("cache-control"),std::string("private")));
    header.push_back(std::make_pair(std::string("date"),std::string("Mon, 21 Oct 2013 20:13:21 GMT")));
    header.push_back(std::make_pair(std::string("location"),std::string("https://www.example.com")));
    hpack = TestHpackEncoder(header, buffans, hpack);

    BufferFromHex("4883 640e ffc1 c0bf", buffans);
    header.push_back(std::make_pair(std::string(":status"),std::string("307")));
    header.push_back(std::make_pair(std::string("cache-control"),std::string("private")));
    header.push_back(std::make_pair(std::string("date"),std::string("Mon, 21 Oct 2013 20:13:21 GMT")));
    header.push_back(std::make_pair(std::string("location"),std::string("https://www.example.com")));
    hpack = TestHpackEncoder(header, buffans, hpack);

    BufferFromHex("88c1 6196 d07a be94 1054 d444 a820 0595", buffans);
    BufferFromHex("040b 8166 e084 a62d 1bff c05a 839b d9ab", buffans);
    BufferFromHex("77ad 94e7 821d d7f2 e6c7 b335 dfdf cd5b", buffans);
    BufferFromHex("3960 d5af 2708 7f36 72c1 ab27 0fb5 291f", buffans);
    BufferFromHex("9587 3160 65c0 03ed 4ee5 b106 3d50 07", buffans);
    header.push_back(std::make_pair(std::string(":status"),std::string("200")));
    header.push_back(std::make_pair(std::string("cache-control"),std::string("private")));
    header.push_back(std::make_pair(std::string("date"),std::string("Mon, 21 Oct 2013 20:13:22 GMT")));
    header.push_back(std::make_pair(std::string("location"),std::string("https://www.example.com")));
    header.push_back(std::make_pair(std::string("content-encoding"),std::string("gzip")));
    header.push_back(std::make_pair(std::string("set-cookie"),std::string("foo=ASDJKHQKBZXOQWEOPIUAXQWEOIU; max-age=3600; version=1")));
    hpack = TestHpackEncoder(header, buffans, hpack);

    printf("Test Hpack Encoder Decoder...\n");
    hpack = std::make_shared<Hpack>();
    header.push_back(std::make_pair(std::string(":scheme"),std::string("w")));
    hpack->Encoder(Hpack::MakeHpWBuffer(buff), header);
    TestHpackDecoder(buff);
    buff.clear();
    header.clear();
    return 0;
}
#endif
unsigned char __HuffmanSwitchfffffff00000b30(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 30;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffffff00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)10;
        break;
    case (0xfffffff40000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)13;
        break;
    case (0xfffffff80000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)22;
        break;
    case (0xfffffffc0000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)256;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffffe00000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffffe00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)249;
        break;
    case (0xfffffff00000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffffff00000b30(rbc, sym);
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffffc00000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffffc00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)127;
        break;
    case (0xffffffd00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)220;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffffa00000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffffa00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)30;
        break;
    case (0xffffffb00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)31;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffff800000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffff800000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)28;
        break;
    case (0xffffff900000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)29;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffff600000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffff600000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)26;
        break;
    case (0xffffff700000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)27;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffff400000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffff400000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)24;
        break;
    case (0xffffff500000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)25;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffff200000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffff200000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)21;
        break;
    case (0xffffff300000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)23;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffff000000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffff000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)19;
        break;
    case (0xffffff100000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)20;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffffee00000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffffee00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)17;
        break;
    case (0xfffffef00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)18;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffffec00000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffffec00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)15;
        break;
    case (0xfffffed00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)16;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffffea00000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffffea00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)12;
        break;
    case (0xfffffeb00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)14;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffffe800000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffffe800000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)8;
        break;
    case (0xfffffe900000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)11;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffffe600000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffffe600000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)6;
        break;
    case (0xfffffe700000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)7;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffffe400000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffffe400000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)4;
        break;
    case (0xfffffe500000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)5;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffffe200000b28(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 28;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffffe200000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)2;
        break;
    case (0xfffffe300000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)3;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffffe000000b27(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 27;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffffe000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)254;
        break;
    case (0xfffffe200000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffffe200000b28(rbc, sym);
    case (0xfffffe400000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffffe400000b28(rbc, sym);
    case (0xfffffe600000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffffe600000b28(rbc, sym);
    case (0xfffffe800000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffffe800000b28(rbc, sym);
    case (0xfffffea00000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffffea00000b28(rbc, sym);
    case (0xfffffec00000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffffec00000b28(rbc, sym);
    case (0xfffffee00000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffffee00000b28(rbc, sym);
    case (0xffffff000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffff000000b28(rbc, sym);
    case (0xffffff200000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffff200000b28(rbc, sym);
    case (0xffffff400000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffff400000b28(rbc, sym);
    case (0xffffff600000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffff600000b28(rbc, sym);
    case (0xffffff800000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffff800000b28(rbc, sym);
    case (0xffffffa00000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffffa00000b28(rbc, sym);
    case (0xffffffc00000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffffc00000b28(rbc, sym);
    case (0xffffffe00000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffffe00000b28(rbc, sym);
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffffc000000b27(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 27;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffffc000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)211;
        break;
    case (0xfffffc200000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)212;
        break;
    case (0xfffffc400000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)214;
        break;
    case (0xfffffc600000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)221;
        break;
    case (0xfffffc800000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)222;
        break;
    case (0xfffffca00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)223;
        break;
    case (0xfffffcc00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)241;
        break;
    case (0xfffffce00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)244;
        break;
    case (0xfffffd000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)245;
        break;
    case (0xfffffd200000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)246;
        break;
    case (0xfffffd400000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)247;
        break;
    case (0xfffffd600000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)248;
        break;
    case (0xfffffd800000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)250;
        break;
    case (0xfffffda00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)251;
        break;
    case (0xfffffdc00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)252;
        break;
    case (0xfffffde00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)253;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffffbc00000b27(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 27;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffffbc00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)203;
        break;
    case (0xfffffbe00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)204;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffffa000000b26(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 26;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffffa000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)218;
        break;
    case (0xfffffa400000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)219;
        break;
    case (0xfffffa800000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)238;
        break;
    case (0xfffffac00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)240;
        break;
    case (0xfffffb000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)242;
        break;
    case (0xfffffb400000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)243;
        break;
    case (0xfffffb800000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)255;
        break;
    case (0xfffffbc00000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffffbc00000b27(rbc, sym);
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffff8000000b26(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 26;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffff8000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)192;
        break;
    case (0xfffff8400000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)193;
        break;
    case (0xfffff8800000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)200;
        break;
    case (0xfffff8c00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)201;
        break;
    case (0xfffff9000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)202;
        break;
    case (0xfffff9400000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)205;
        break;
    case (0xfffff9800000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)210;
        break;
    case (0xfffff9c00000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)213;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffff6000000b25(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 25;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffff6000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)199;
        break;
    case (0xfffff6800000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)207;
        break;
    case (0xfffff7000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)234;
        break;
    case (0xfffff7800000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)235;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffff4000000b24(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 24;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffff4000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)236;
        break;
    case (0xfffff5000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)237;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffff2000000b24(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 24;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffff2000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)215;
        break;
    case (0xfffff3000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)225;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffff0000000b24(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 24;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffff0000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)171;
        break;
    case (0xfffff1000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)206;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffee000000b24(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 24;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffee000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)148;
        break;
    case (0xffffef000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)159;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffec000000b24(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 24;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffec000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)144;
        break;
    case (0xffffed000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)145;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffea000000b24(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 24;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffea000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)9;
        break;
    case (0xffffeb000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)142;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffe0000000b23(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 23;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffe0000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)188;
        break;
    case (0xffffe2000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)191;
        break;
    case (0xffffe4000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)197;
        break;
    case (0xffffe6000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)231;
        break;
    case (0xffffe8000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)239;
        break;
    case (0xffffea000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffea000000b24(rbc, sym);
    case (0xffffec000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffec000000b24(rbc, sym);
    case (0xffffee000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffee000000b24(rbc, sym);
    case (0xfffff0000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffff0000000b24(rbc, sym);
    case (0xfffff2000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffff2000000b24(rbc, sym);
    case (0xfffff4000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffff4000000b24(rbc, sym);
    case (0xfffff6000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffff6000000b25(rbc, sym);
    case (0xfffff8000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffff8000000b26(rbc, sym);
    case (0xfffffa000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffffa000000b26(rbc, sym);
    case (0xfffffc000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffffc000000b27(rbc, sym);
    case (0xfffffe000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffffe000000b27(rbc, sym);
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffc0000000b23(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 23;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffc0000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)147;
        break;
    case (0xffffc2000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)149;
        break;
    case (0xffffc4000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)150;
        break;
    case (0xffffc6000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)151;
        break;
    case (0xffffc8000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)152;
        break;
    case (0xffffca000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)155;
        break;
    case (0xffffcc000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)157;
        break;
    case (0xffffce000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)158;
        break;
    case (0xffffd0000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)165;
        break;
    case (0xffffd2000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)166;
        break;
    case (0xffffd4000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)168;
        break;
    case (0xffffd6000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)174;
        break;
    case (0xffffd8000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)175;
        break;
    case (0xffffda000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)180;
        break;
    case (0xffffdc000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)182;
        break;
    case (0xffffde000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)183;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffbc000000b23(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 23;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffbc000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)141;
        break;
    case (0xffffbe000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)143;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffb8000000b23(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 23;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffb8000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)139;
        break;
    case (0xffffba000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)140;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffb4000000b23(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 23;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffb4000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)137;
        break;
    case (0xffffb6000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)138;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffb0000000b23(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 23;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffb0000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)1;
        break;
    case (0xffffb2000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)135;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffffa0000000b22(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 22;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffffa0000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)198;
        break;
    case (0xffffa4000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)228;
        break;
    case (0xffffa8000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)232;
        break;
    case (0xffffac000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)233;
        break;
    case (0xffffb0000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffb0000000b23(rbc, sym);
    case (0xffffb4000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffb4000000b23(rbc, sym);
    case (0xffffb8000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffb8000000b23(rbc, sym);
    case (0xffffbc000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffbc000000b23(rbc, sym);
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffff80000000b22(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 22;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffff80000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)178;
        break;
    case (0xffff84000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)181;
        break;
    case (0xffff88000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)185;
        break;
    case (0xffff8c000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)186;
        break;
    case (0xffff90000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)187;
        break;
    case (0xffff94000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)189;
        break;
    case (0xffff98000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)190;
        break;
    case (0xffff9c000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)196;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffff60000000b22(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 22;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffff60000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)154;
        break;
    case (0xffff64000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)156;
        break;
    case (0xffff68000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)160;
        break;
    case (0xffff6c000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)163;
        break;
    case (0xffff70000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)164;
        break;
    case (0xffff74000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)169;
        break;
    case (0xffff78000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)170;
        break;
    case (0xffff7c000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)173;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffff58000000b22(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 22;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffff58000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)136;
        break;
    case (0xffff5c000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)146;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffff50000000b22(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 22;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffff50000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)133;
        break;
    case (0xffff54000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)134;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffff48000000b22(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 22;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffff48000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)129;
        break;
    case (0xffff4c000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)132;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffff40000000b21(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 21;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffff40000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)230;
        break;
    case (0xffff48000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffff48000000b22(rbc, sym);
    case (0xffff50000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffff50000000b22(rbc, sym);
    case (0xffff58000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffff58000000b22(rbc, sym);
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffff20000000b21(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 21;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffff20000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)216;
        break;
    case (0xffff28000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)217;
        break;
    case (0xffff30000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)227;
        break;
    case (0xffff38000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)229;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffff00000000b21(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 21;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffff00000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)176;
        break;
    case (0xffff08000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)177;
        break;
    case (0xffff10000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)179;
        break;
    case (0xffff18000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)209;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffee0000000b21(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 21;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffee0000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)153;
        break;
    case (0xfffee8000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)161;
        break;
    case (0xfffef0000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)167;
        break;
    case (0xfffef8000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)172;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffec0000000b20(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 20;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffec0000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)224;
        break;
    case (0xfffed0000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)226;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffea0000000b20(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 20;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffea0000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)184;
        break;
    case (0xfffeb0000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)194;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffe80000000b20(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 20;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffe80000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)131;
        break;
    case (0xfffe90000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)162;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffe60000000b20(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 20;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffe60000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)128;
        break;
    case (0xfffe70000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)130;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfffe00000000b19(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 19;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfffe00000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)92;
        break;
    case (0xfffe20000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)195;
        break;
    case (0xfffe40000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)208;
        break;
    case (0xfffe60000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffe60000000b20(rbc, sym);
    case (0xfffe80000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffe80000000b20(rbc, sym);
    case (0xfffea0000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffea0000000b20(rbc, sym);
    case (0xfffec0000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffec0000000b20(rbc, sym);
    case (0xfffee0000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffee0000000b21(rbc, sym);
    case (0xffff00000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffff00000000b21(rbc, sym);
    case (0xffff20000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffff20000000b21(rbc, sym);
    case (0xffff40000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffff40000000b21(rbc, sym);
    case (0xffff60000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffff60000000b22(rbc, sym);
    case (0xffff80000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffff80000000b22(rbc, sym);
    case (0xffffa0000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffa0000000b22(rbc, sym);
    case (0xffffc0000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffc0000000b23(rbc, sym);
    case (0xffffe0000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffffe0000000b23(rbc, sym);
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfff800000000b15(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 15;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfff800000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)60;
        break;
    case (0xfffa00000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)96;
        break;
    case (0xfffc00000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)123;
        break;
    case (0xfffe00000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfffe00000000b19(rbc, sym);
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfff000000000b14(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 14;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfff000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)94;
        break;
    case (0xfff400000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)125;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffc000000000b13(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 13;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffc000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)0;
        break;
    case (0xffc800000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)36;
        break;
    case (0xffd000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)64;
        break;
    case (0xffd800000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)91;
        break;
    case (0xffe000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)93;
        break;
    case (0xffe800000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)126;
        break;
    case (0xfff000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfff000000000b14(rbc, sym);
    case (0xfff800000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfff800000000b15(rbc, sym);
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchffa000000000b12(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 12;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xffa000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)35;
        break;
    case (0xffb000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)62;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchff8000000000b11(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 11;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xff8000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)124;
        break;
    case (0xffa000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffa000000000b12(rbc, sym);
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchff4000000000b11(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 11;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xff4000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)39;
        break;
    case (0xff6000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)43;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchff0000000000b10(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 10;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xff0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)63;
        break;
    case (0xff4000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchff4000000000b11(rbc, sym);
    case (0xff8000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchff8000000000b11(rbc, sym);
    case (0xffc000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchffc000000000b13(rbc, sym);
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchfe0000000000b10(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 10;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xfe0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)33;
        break;
    case (0xfe4000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)34;
        break;
    case (0xfe8000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)40;
        break;
    case (0xfec000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)41;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchf80000000000b8(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 8;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xf80000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)38;
        break;
    case (0xf90000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)42;
        break;
    case (0xfa0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)44;
        break;
    case (0xfb0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)59;
        break;
    case (0xfc0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)88;
        break;
    case (0xfd0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)90;
        break;
    case (0xfe0000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchfe0000000000b10(rbc, sym);
    case (0xff0000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchff0000000000b10(rbc, sym);
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchf00000000000b7(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 7;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xf00000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)119;
        break;
    case (0xf20000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)120;
        break;
    case (0xf40000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)121;
        break;
    case (0xf60000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)122;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitche80000000000b7(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 7;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xe80000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)106;
        break;
    case (0xea0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)107;
        break;
    case (0xec0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)113;
        break;
    case (0xee0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)118;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitche00000000000b7(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 7;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xe00000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)85;
        break;
    case (0xe20000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)86;
        break;
    case (0xe40000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)87;
        break;
    case (0xe60000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)89;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchd80000000000b7(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 7;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xd80000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)81;
        break;
    case (0xda0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)82;
        break;
    case (0xdc0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)83;
        break;
    case (0xde0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)84;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchd00000000000b7(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 7;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xd00000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)77;
        break;
    case (0xd20000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)78;
        break;
    case (0xd40000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)79;
        break;
    case (0xd60000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)80;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchc80000000000b7(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 7;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xc80000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)73;
        break;
    case (0xca0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)74;
        break;
    case (0xcc0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)75;
        break;
    case (0xce0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)76;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchc00000000000b7(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 7;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xc00000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)69;
        break;
    case (0xc20000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)70;
        break;
    case (0xc40000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)71;
        break;
    case (0xc60000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)72;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchb80000000000b7(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 7;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xb80000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)58;
        break;
    case (0xba0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)66;
        break;
    case (0xbc0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)67;
        break;
    case (0xbe0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)68;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitchb00000000000b6(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 6;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xb00000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)114;
        break;
    case (0xb40000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)117;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitcha80000000000b6(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 6;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xa80000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)110;
        break;
    case (0xac0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)112;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitcha00000000000b6(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 6;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0xa00000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)108;
        break;
    case (0xa40000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)109;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitch980000000000b6(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 6;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0x980000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)103;
        break;
    case (0x9c0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)104;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitch900000000000b6(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 6;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0x900000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)100;
        break;
    case (0x940000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)102;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitch880000000000b6(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 6;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0x880000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)95;
        break;
    case (0x8c0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)98;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitch800000000000b6(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 6;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0x800000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)61;
        break;
    case (0x840000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)65;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitch780000000000b6(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 6;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0x780000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)56;
        break;
    case (0x7c0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)57;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitch700000000000b6(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 6;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0x700000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)54;
        break;
    case (0x740000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)55;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitch680000000000b6(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 6;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0x680000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)52;
        break;
    case (0x6c0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)53;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitch600000000000b6(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 6;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0x600000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)47;
        break;
    case (0x640000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)51;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitch580000000000b6(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 6;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0x580000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)45;
        break;
    case (0x5c0000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)46;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitch500000000000b6(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 6;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0x500000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)32;
        break;
    case (0x540000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)37;
        break;
    }
    return HpRBufferCache::CacheSuccess;
}


unsigned char __HuffmanSwitch0b5(HpRBufferCache &rbc,Integer2byte &sym) {
    unsigned char rbcstat;
    const unsigned char thisbit = 5;
    if ((rbcstat = rbc.ReadUntilBit(thisbit)) != HpRBufferCache::CacheSuccess) {
        return rbcstat;
    }
    switch(rbc.HuffmanCode()) {
    case (0x0ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)48;
        break;
    case (0x80000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)49;
        break;
    case (0x100000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)50;
        break;
    case (0x180000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)97;
        break;
    case (0x200000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)99;
        break;
    case (0x280000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)101;
        break;
    case (0x300000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)105;
        break;
    case (0x380000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)111;
        break;
    case (0x400000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)115;
        break;
    case (0x480000000000ul << (BitOfUinteger5Byte - 48)):
        sym = (Integer2byte)116;
        break;
    case (0x500000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitch500000000000b6(rbc, sym);
    case (0x580000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitch580000000000b6(rbc, sym);
    case (0x600000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitch600000000000b6(rbc, sym);
    case (0x680000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitch680000000000b6(rbc, sym);
    case (0x700000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitch700000000000b6(rbc, sym);
    case (0x780000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitch780000000000b6(rbc, sym);
    case (0x800000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitch800000000000b6(rbc, sym);
    case (0x880000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitch880000000000b6(rbc, sym);
    case (0x900000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitch900000000000b6(rbc, sym);
    case (0x980000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitch980000000000b6(rbc, sym);
    case (0xa00000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitcha00000000000b6(rbc, sym);
    case (0xa80000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitcha80000000000b6(rbc, sym);
    case (0xb00000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchb00000000000b6(rbc, sym);
    case (0xb80000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchb80000000000b7(rbc, sym);
    case (0xc00000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchc00000000000b7(rbc, sym);
    case (0xc80000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchc80000000000b7(rbc, sym);
    case (0xd00000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchd00000000000b7(rbc, sym);
    case (0xd80000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchd80000000000b7(rbc, sym);
    case (0xe00000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitche00000000000b7(rbc, sym);
    case (0xe80000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitche80000000000b7(rbc, sym);
    case (0xf00000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchf00000000000b7(rbc, sym);
    case (0xf80000000000ul << (BitOfUinteger5Byte - 48)):
        return __HuffmanSwitchf80000000000b8(rbc, sym);
    }
    return HpRBufferCache::CacheSuccess;
}

