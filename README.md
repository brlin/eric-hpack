### eric-hpack
遵守 std c++11 的 hpack 實作

``` bash
g++ --std=c++11 -o2 hpack.cpp hpack.h
```

所有的實作都在 hpack.cpp, hpack.h 只有要公開的 class 宣告
建議複製  hpack.cpp, hpack.h 到專案文件夾中直接使用
##### Config
###### 旗標
UNITTEST
> 會產生 main 函數並執行單元測試，需要 DEBUGFLAG

DEBUGFLAG
> 會編譯 debug 用的函式及加入檢查狀態用的判斷式，會引響執行速度，狀態異常時用 printf 顯示
##### 常數
- `bool DecodeStringLiteralOctet;` 
> 是否要解碼 Octet String Literal
```c
enum StringLiteralEncodeWay {
    EOctet,
    EHuffman,
    EShortest
};
```
- `const StringLiteralEncodeWay DefaultStringLiteralEncode;`
> 在編碼時要選擇何種編碼方式，EShortest 會選兩種中最短的，用以解決 Huffman 編碼在最糟情況下比部編碼還長的問題
> 程式中用 Template 實作，幾乎沒有額外開銷
- `bool HpackDecoderCheckParameter`
> 是否要檢查 Hpack::Decoder 的參數是否合理 
- `bool LiteralDecodeCheckLiteralPad`
> 是否要檢查 LiteralDecode 到字串結尾的狀態

#### Hpack
```c
HpackStatus Decoder(std::shared_ptr<HpRBuffer> stream, std::vector<KeyValPair> &header);
```
stream 供讀取的 HpRBuffer，如果 stream 長度為零時，HpackDecoderCheckParameter 為真時會返 HpackStatus::RBuffZeroLength，為假時為未定義行為


#### HpRBuffer
```c
class HpRBuffer {
    // 0 length not allowed
  public:
    virtual ~HpRBuffer() = default;
    virtual bool Next() =0;
    virtual size_t Size() = 0;
    virtual unsigned char Currrent() = 0;
};
```

歡迎實施自己的 HpRBuffer ，HpRBuffer 在 decode 流程中使用
Next() 在異常及結尾時皆回傳 False，必須擔保 Next() 在回傳 False 之後再呼叫必須一直回傳False
Currrent() 必須在建構之後到 Next() 回傳 False 前可用，所以不可以出現空的 buffer，Next() 回傳 False 之後可以回傳任意值
不用擔保 HpRBuffer 是否反映在建構式呼叫到第一次任意函式呼叫間的底層狀態反映
VecHpRBuffer 底層改動會反映，ArrHpRBuffer 長度不會值會
#### Type Size
```c
typedef int_fast16_t Integer2byte;
typedef uint_fast64_t Uinteger5byte;
```
- `enum` 用預設的 `int`
- `size_t` large then 16 bit
- `Integer2byte`  large then 16 bit, 在程式中表達 IndexTable 及 Buffer 等的最大限制及 sym, 需要自少可以表示 -1
- `unsigned char` Must 8 bit
- `Uinteger5byte` large then 46 bit, multiples of `unsigned char`

SRTINGLITERAL 要求要用 1 填充 Pading 到 Octet 邊緣

#### 違背標準
1. StringLiteral 解碼時標準要求最末填充只能是 1 和填充不可以超過 7 bit 
        LiteralPadNotEOSMsb,
        LiteralPadLongThan7Bit,
沒做
2. 表上會有多個相同的 key 但 value 不同，標準沒說如只有 key 相同的情況下， index要選哪個，但標準的範例中選 index 最小值，我也跟著
StaticTable has priority in search. If two tables have the same key, StaticTable will be used.
3. DefaultStringLiteralEncode 設為 EShortest 時
4. 

#### Performance Tuning 
Performance tuning 在 `-O1` 的環境下測試
測試後發現 `-O, -O1, -O2` 並沒有具體的速度提升
測試環境 i5-10300H Windows10 連接充電器 Mingw
DefaultStringLiteralEncode = EShortest
##### Base Line On Git 337b986054a9f99674b581dd72cf0501b475f657 
`gcc -O1`
```
With Error
```
VM Ubuntu 22.04 VCPU 2 Memory 3GB
```
OctetAll run 250 time in 1.160000ms.
LargeKvp run 250 time in 516.680000ms.
bench/template_parameters.data run 250 time in 6.280000ms.
bench/Gophercon.data run 250 time in 232.480000ms.
bench/Hlp.data run 250 time in 433.440000ms.
bench/Cppvec.data run 250 time in 294.720000ms.
bench/content_type.data run 250 time in 522.560000ms.
bench/example_domains.data run 250 time in 1.160000ms.
bench/clang_command.data run 250 time in 1.000000ms.
bench/cc_sa.data run 250 time in 245.440000ms.
bench/go_http.data run 250 time in 1812.960000ms.
bench/HttplightSitemap.data run 250 time in 180.680000ms.
bench/NginxSitemap.data run 250 time in 2125.600000ms.
bench/HaproxySitemap.data run 250 time in 1057.800000ms.

```
##### 重構 Hpack On Git 215a5f346b791ce28fd979ab906c4564487edf1e
`gcc -O1`
```
OctetAll run 250 time in 1.000000ms.
LargeKvp run 250 time in 618.200000ms.
bench/template_parameters.data run 250 time in 6.000000ms.
bench/Gophercon.data run 250 time in 254.320000ms.
bench/Hlp.data run 250 time in 469.440000ms.
bench/Cppvec.data run 250 time in 348.160000ms.
bench/content_type.data run 250 time in 572.800000ms.
bench/example_domains.data run 250 time in 2.000000ms.
bench/clang_command.data run 250 time in 1.000000ms.
bench/cc_sa.data run 250 time in 268.880000ms.
bench/go_http.data run 250 time in 1978.560000ms.
bench/HttplightSitemap.data run 250 time in 225.840000ms.
bench/NginxSitemap.data run 250 time in 2450.040000ms.
bench/HaproxySitemap.data run 250 time in 1263.400000ms.
```
VM Ubuntu 22.04 VCPU 2 Memory 3GB
```
OctetAll run 250 time in 0.440000ms.  
LargeKvp run 250 time in 526.080000ms.  
bench/template_parameters.data run 250 time in 5.040000ms.  
bench/Gophercon.data run 250 time in 227.320000ms.  
bench/Hlp.data run 250 time in 423.640000ms.  
bench/Cppvec.data run 250 time in 285.280000ms.  
bench/content_type.data run 250 time in 508.400000ms.  
bench/example_domains.data run 250 time in 1.000000ms.  
bench/clang_command.data run 250 time in 1.000000ms.  
bench/cc_sa.data run 250 time in 236.280000ms.  
bench/go_http.data run 250 time in 1746.920000ms.  
bench/HttplightSitemap.data run 250 time in 176.320000ms.  
bench/NginxSitemap.data run 250 time in 2024.520000ms.  
bench/HaproxySitemap.data run 250 time in 1004.160000ms.
```
- 用 `template<Bool SLEF>` 移除執行期分支
- `Hpack::_DecoderLoop` 發現 headerfield, kvp 有在迴圈中複製，用參照及 clear 加速
##### CRTP
```
OctetAll run 250 time in 265.666667ms.
LargeKvp run 250 time in 448.333333ms.
bench/template_parameters.data run 250 time in 5.333333ms.
bench/Gophercon.data run 250 time in 215.333333ms.
bench/Hlp.data run 250 time in 404.333333ms.
bench/Cppvec.data run 250 time in 304.333333ms.
bench/content_type.data run 250 time in 495.666667ms.
bench/example_domains.data run 250 time in 1.000000ms.
bench/clang_command.data run 250 time in 1.000000ms.
bench/cc_sa.data run 250 time in 241.000000ms.
bench/go_http.data run 250 time in 1822.666667ms.
bench/HttplightSitemap.data run 250 time in 192.333333ms.
bench/NginxSitemap.data run 250 time in 2135.000000ms.
bench/HaproxySitemap.data run 250 time in 1048.333333ms.
```
- 幾乎沒有引響